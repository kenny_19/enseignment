<?php



//ce fonction pour verifier un valeur existe dans les donées d'un table
function existe($table,$valeurAttribut1,$valeurAttribut2){
	//au debut on sait pas les données existe
	$existe=false;
	$servername = "localhost";
	$username = "root";
	$password = "123456";
	 
	$dbname = "enseignement";
	// créer connection
	$conn = mysqli_connect($servername, $username, $password, $dbname);
	// tester connection
	// tester connection
	if (!$conn) {
		die("Connection failed: " . mysqli_connect_error());
	}
	if($table=="COMPOSANTE"){
		$sql = "SELECT * FROM COMPOSANTE WHERE ID_COMP='{$valeurAttribut1}'";
	 }else if($table=="CATEGORIES"){
		$sql = "SELECT * FROM CATEGORIES WHERE NO_CAT='{$valeurAttribut1}'";
	 }else if($table=="DIPLOME"){
		$sql = "SELECT * FROM DIPLOME WHERE CODE_DIPLOME='{$valeurAttribut1}' AND VDI='{$valeurAttribut2}'";
	 }else if($table=="EC"){
		$sql = "SELECT * FROM EC WHERE CODE_EC='{$valeurAttribut1}'";
	 }else if($table=="ENSEIGNANT"){
		$sql = "SELECT * FROM ENSEIGNANT WHERE ID_ENS='{$valeurAttribut1}'";
	 }else if($table=="ENSEIGNEMENT"){
		$sql = "SELECT * FROM ENSEIGNEMENT WHERE CODE_EC='{$valeurAttribut1}' AND ANNEE='{$valeurAttribut2}'";
	 }else if($table=='ETAPE'){
			$sql = "SELECT * FROM ETAPE WHERE CODE_ETAPE='{$valeurAttribut1}' AND VET='{$valeurAttribut2}'";
	 }else if($table=='SEMESTRE'){
			$sql = "SELECT * FROM SEMESTRE WHERE CODE_SEM='{$valeurAttribut1}' ";
	 }else if($table=='SERVICE'){
		 //NE CONSULTER PAS SERVICE
			$sql = "SELECT * FROM SERVICE";
	 }else if($table=='UE'){
			$sql = "SELECT * FROM UE WHERE CODE_UE='{$valeurAttribut1}";
	 }else if($table=='SERVICE_ANNUEL'){
		 //NE CONSULTER PAS SERVICE_ANNUEL
			$sql = "SELECT * FROM SERVICE_ANNUEL";
	 }
	
	if($table=="COMPOSANTE"||$table=="CATEGORIES"||$table=="DIPLOME"||$table=="EC"||$table=="ENSEIGNANT"||$table=="ENSEIGNEMENT"||$table=='ETAPE'||$table=='SEMESTRE'||$table=='UE'){
		 $result = $conn->query($sql);
		if ($result->num_rows > 0) {
			$existe=true;
		}
	}
	mysqli_close($conn);
	return $existe;
}
//ce fonction pour tourner un durée scolaire
function anneeScolaireEnlongeur($anneeDebut,$longeurAnnee){
	
	$anneeDebut=intval($anneeDebut);
	$anneeFin=$anneeDebut+intval($longeurAnnee);
	return strval($anneeDebut).'-'.strval($anneeFin);
}

function anneeScolaire($anneeDebut,$anneeFin){
	return strval($anneeDebut).'-'.strval($anneeFin);
}

function debAnnee($dureeAnneScolaire){
	return intval(substr($dureeAnneScolaire,0,4));
}

function finAnnee($dureeAnneScolaire){
	return intval(substr($dureeAnneScolaire,5));
}


function dureeAnne($dureeAnneScolaire){
	return finAnnee($dureeAnneScolaire)-debAnnee($dureeAnneScolaire);
}
//consulter les donnees dans diploma en annuel
function diplomeEnAnne($dureeAnneScolaire){
	//au debut on sait pas les données existe
	$existe=false;
	$servername = "localhost";
	$username = "root";
	$password = "123456";
	 
	$dbname = "enseignement";
	// créer connection
	$conn = mysqli_connect($servername, $username, $password, $dbname);
	// tester connection
	//afficher les chose comme é
	mysqli_query($conn , "set names utf8");
	if (!$conn) {
		die("Connection failed: " . mysqli_connect_error());
	}
	$anneeDeb=debAnnee($dureeAnneScolaire);
	$anneeFin=finAnnee($dureeAnneScolaire);
	
	$sql = "SELECT * FROM DIPLOME WHERE ANNEE_DEB='{$anneeDeb}' AND ANNEE_FIN='{$anneeFin}'";
	 $result = $conn->query($sql);
	if ($result->num_rows > 0) {
		while($row = $result->fetch_assoc()) {
			echo "<a href=./gestionEnObjet/gsDIploma.php?CODE_DIPLOME={$row["CODE_DIPLOME"] }&VDI={$row["VDI"] }>"."CODE_DIPLOME: " . $row["CODE_DIPLOME"]. " - LIBELLE_DIPLOME: " . $row["LIBELLE_DIPLOME"]. " - VDI: " . $row["VDI"]. " - LIBELLE_VDI: " . $row["LIBELLE_VDI"]. " - ANNEE_DEB: " . $row["ANNEE_DEB"]. " - ANNEE_FIN: " . $row["ANNEE_FIN"]. "</a>"."</br>";
		}
	}else{
		echo "</br>"."0 resultat en diploma"."</br>";
	}
	
	mysqli_close($conn);
}
//consulter les donnees dans enseignement en annuel
function enseignmentEnAnne($dureeAnneScolaire){
	//au debut on sait pas les données existe
	$existe=false;
	$servername = "localhost";
	$username = "root";
	$password = "123456";
	 
	$dbname = "enseignement";
	// créer connection
	$conn = mysqli_connect($servername, $username, $password, $dbname);
	// tester connection
	//afficher les chose comme é
	mysqli_query($conn , "set names utf8");
	if (!$conn) {
		die("Connection failed: " . mysqli_connect_error());
	}
	
	
	$sql = "SELECT * FROM ENSEIGNEMENT WHERE ANNEE='{$dureeAnneScolaire}' ";
	 $result = $conn->query($sql);
	if ($result->num_rows > 0) {
		while($row = $result->fetch_assoc()) {
			echo "<a href=./gestionEnObjet/gsEnseignment.php?CODE_EC={$row["CODE_EC"] }&ANNEE={$row["ANNEE"] }>"."CODE_EC: " . $row["CODE_EC"]. " - ANNEE: " . $row["ANNEE"]. " - EFF_PREV: " . $row["EFF_PREV"]. " - EFF_REEL: " . $row["EFF_REEL"]. " - GPCM: " . $row["GPCM"]. " - GPEI: " . $row["GPEI"]. " - GPTD: " . $row["GPTD"]. " - GPTP: " . $row["GPTP"]. " - GPTPL: " . $row["GPTPL"]. " - GPPRJ: " . $row["GPPRJ"]. " - GPCMSER: " . $row["GPCMSER"]. " - GPEISER: " . $row["GPEISER"]. " - GPTDSER: " . $row["GPTDSER"]. " - GPTPSER: " . $row["GPTPSER"]. " - GPTPLSER: " . $row["GPTPLSER"]. " - GPPRJSER: " . $row["GPPRJSER"]. "</a>"."</br>";
		}
	}else{
		echo "</br>"."0 resultat en enseignement"."</br>";
	}
	
	mysqli_close($conn);
}

?>
