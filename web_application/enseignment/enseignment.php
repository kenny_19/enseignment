<?php
$servername = "localhost";
$username = "root";
$password = "123456";
 
//creer connection
$conn = new mysqli($servername, $username, $password);
 
// tester connection
if ($conn->connect_error) {
    die("Perdu connection: " . $conn->connect_error);
} 
echo "reussir connection";
mysqli_close($conn);
?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>gestion_enseignement</title>
</head>
<body>
	</br>
	<i>gestion_enseignement</i>
	</br>
	<a href="index.php">retourner_au_main_page</a>
	</br>
	</br>
		<i>consulter les tableux</i>
		</br>
		<form method="post" action="consulter.php" >
		<fieldset>
				<legend>Tableux_a_consulter</legend>
				<br/>
				Table :
				<input name="table" list="table" />
				<datalist id="table">
					<option value="CATEGORIES" />
					<option value="COMPOSANTE" />
					<option value="DIPLOME" />
					<option value="EC" />
					<option value="ENSEIGNANT" />
					<option value="ENSEIGNEMENT" />
					<option value="ETAPE" />
					<option value="SEMESTRE" />
					<option value="SERVICE" />
					<option value="SERVICE_ANNUEL" />
					<option value="UE" />
				</datalist> 	
	
		</fieldset>
		<br/>
		<input type="submit" name="submit" value="consulter" />
		</form>
		<br/>
		<i>inserer les donées dans les tableaux</i>
		<br/>
		<form method="post" action="ajouter.php" >
		<fieldset>
				<legend>Tableux_a_insérer</legend>
				<br/>
				Table :
				<input name="table_ajouter" list="table_ajouter" />
				<datalist id="table_ajouter">
					<option value="COMPOSANTE" />
					<option value="EC" />
					<option value="ENSEIGNEMENT" />
					<option value="ETAPE" />
					<option value="SEMESTRE" />
					<option value="UE" />
				</datalist> 	
	
		</fieldset>
		<br/>
		<input type="submit" name="submit" value="ajouter" />
		</form>
		
		<br/>
		<i>supprimer les donées dans les tableaux</i>
		<br/>
		<form method="post" action="supprime.php" >
		<fieldset>
				<legend>Tableux_a_supprime</legend>
				<br/>
				Table :
				<input name="table_supprime" list="table_supprime" />
				<datalist id="table_supprime">
					<option value="COMPOSANTE" />
					<option value="EC" />
					<option value="ENSEIGNEMENT" />
					<option value="ETAPE" />
					<option value="SEMESTRE" />
					<option value="UE" />
				</datalist> 	
	
		</fieldset>
		<br/>
		<input type="submit" name="submit" value="suprime" />
		</form>
		
		<br/>
		<i>mis à jour les donées dans les tableaux</i>
		<br/>
		<form method="post" action="update.php" >
		<fieldset>
				<legend>Tableux_maj</legend>
				<br/>
				Table :
				<input name="table_maj" list="table_maj" />
				<datalist id="table_maj">
					<option value="COMPOSANTE" />
					<option value="EC" />
					<option value="ENSEIGNEMENT" />
					<option value="ETAPE" />
					<option value="SEMESTRE" />
					<option value="UE" />
				</datalist> 	
	
		</fieldset>
		<br/>
		<input type="submit" name="submit" value="maj" />
		</form>

</body>
</html>