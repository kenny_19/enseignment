--------------------------------------------------------
--  Fichier cr�� - mercredi-avril-01-2020   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Table CATEGORIES
--------------------------------------------------------

  CREATE TABLE "CATEGORIES" 
   (	"NO_CAT" NUMBER(2,0), 
	"CATEGORIE" VARCHAR2(100)
   ) ;
--------------------------------------------------------
--  DDL for Table COMPOSANTE
--------------------------------------------------------

  CREATE TABLE "COMPOSANTE" 
   (	"ID_COMP" CHAR(3), 
	"NOM_COMP" VARCHAR2(30), 
	"LIEU_COMP" VARCHAR2(30) DEFAULT NULL
   ) ;
--------------------------------------------------------
--  DDL for Table DIPLOME
--------------------------------------------------------

  CREATE TABLE "DIPLOME" 
   (	"CODE_DIPLOME" VARCHAR2(10), 
	"LIBELLE_DIPLOME" VARCHAR2(100), 
	"VDI" NUMBER(3,0), 
	"LIBELLE_VDI" VARCHAR2(50), 
	"ANNEE_DEB" NUMBER(4,0) DEFAULT 2018, 
	"ANNEE_FIN" NUMBER(4,0) DEFAULT 2023
   ) ;
--------------------------------------------------------
--  DDL for Table EC
--------------------------------------------------------

  CREATE TABLE "EC" 
   (	"CODE_EC" VARCHAR2(10), 
	"LIBELLE_EC" VARCHAR2(100), 
	"NATURE_EC" CHAR(1) DEFAULT NULL, 
	"HCM" NUMBER(3,0) DEFAULT NULL, 
	"HTD" NUMBER(3,0) DEFAULT NULL, 
	"HTP" NUMBER(3,0) DEFAULT NULL, 
	"HEI" NUMBER(3,0) DEFAULT NULL, 
	"HPRJ" NUMBER(3,0) DEFAULT NULL, 
	"HTPL" NUMBER(3,0) DEFAULT NULL, 
	"CNU" NUMBER(4,0) DEFAULT NULL, 
	"NO_CAT" NUMBER(2,0) DEFAULT NULL, 
	"CODE_EC_PERE" VARCHAR2(10) DEFAULT NULL, 
	"CODE_UE" VARCHAR2(10) DEFAULT NULL
   ) ;
--------------------------------------------------------
--  DDL for Table ENSEIGNANT
--------------------------------------------------------

  CREATE TABLE "ENSEIGNANT" 
   (	"ID_ENS" VARCHAR2(4), 
	"NOM" VARCHAR2(15), 
	"PRENOM" VARCHAR2(15), 
	"FONCTION" VARCHAR2(15), 
	"HOBLIG" NUMBER(5,2), 
	"HMAX" NUMBER(5,2), 
	"CRCT" CHAR(3), 
	"PES_PEDR" CHAR(3), 
	"ID_COMP" CHAR(3)
   ) ;
--------------------------------------------------------
--  DDL for Table ENSEIGNEMENT
--------------------------------------------------------

  CREATE TABLE "ENSEIGNEMENT" 
   (	"CODE_EC" VARCHAR2(10), 
	"ANNEE" VARCHAR2(9), 
	"EFF_PREV" NUMBER(4,0), 
	"EFF_REEL" NUMBER(4,0), 
	"GPCM" NUMBER(2,0) DEFAULT 0, 
	"GPEI" NUMBER(2,0) DEFAULT 0, 
	"GPTD" NUMBER(2,0) DEFAULT 0, 
	"GPTP" NUMBER(2,0) DEFAULT 0, 
	"GPTPL" NUMBER(2,0) DEFAULT 0, 
	"GPPRJ" NUMBER(2,0) DEFAULT 0, 
	"GPCMSER" NUMBER(4,2) DEFAULT 0, 
	"GPEISER" NUMBER(4,2) DEFAULT 0, 
	"GPTDSER" NUMBER(4,2) DEFAULT 0, 
	"GPTPSER" NUMBER(4,2) DEFAULT 0, 
	"GPTPLSER" NUMBER(4,2) DEFAULT 0, 
	"GPPRJSER" NUMBER(4,2) DEFAULT 0
   ) ;
--------------------------------------------------------
--  DDL for Table ETAPE
--------------------------------------------------------

  CREATE TABLE "ETAPE" 
   (	"CODE_ETAPE" VARCHAR2(10), 
	"VET" NUMBER(3,0), 
	"LIBELLE_VET" VARCHAR2(100), 
	"ID_COMP" CHAR(3), 
	"CODE_DIPLOME" VARCHAR2(10), 
	"VDI" NUMBER(3,0)
   ) ;
--------------------------------------------------------
--  DDL for Table SEMESTRE
--------------------------------------------------------

  CREATE TABLE "SEMESTRE" 
   (	"CODE_SEM" VARCHAR2(10), 
	"LIBELLE_SEM" VARCHAR2(50), 
	"NO_SEM" NUMBER(2,0), 
	"CODE_ETAPE" VARCHAR2(10) DEFAULT NULL, 
	"VET" NUMBER(3,0) DEFAULT NULL
   ) ;
--------------------------------------------------------
--  DDL for Table SERVICE
--------------------------------------------------------

  CREATE TABLE "SERVICE" 
   (	"ID_ENS" VARCHAR2(4), 
	"CODE_EC" VARCHAR2(10), 
	"ANNEE" VARCHAR2(9), 
	"NBGPCM" NUMBER(4,2) DEFAULT 0, 
	"NBGPEI" NUMBER(4,2) DEFAULT 0, 
	"NBGPTP" NUMBER(4,2) DEFAULT 0, 
	"NBGPTPL" NUMBER(4,2) DEFAULT 0, 
	"NBGPPRJ" NUMBER(4,2) DEFAULT 0, 
	"HEQTD" NUMBER(4,2) DEFAULT 0
   ) ;
--------------------------------------------------------
--  DDL for Table UE
--------------------------------------------------------

  CREATE TABLE "UE" 
   (	"CODE_UE" VARCHAR2(10), 
	"LIBELLE_UE" VARCHAR2(100), 
	"NATURE_UE" CHAR(1) DEFAULT NULL, 
	"ECTS" NUMBER(2,0) DEFAULT NULL, 
	"CODE_SEM" VARCHAR2(10) DEFAULT NULL, 
	"CODE_UE_PERE" VARCHAR2(10) DEFAULT NULL
   ) ;
REM INSERTING into CATEGORIES
SET DEFINE OFF;
Insert into CATEGORIES (NO_CAT,CATEGORIE) values ('1','Math�matiques et outils math�matiques pour l''informatique');
Insert into CATEGORIES (NO_CAT,CATEGORIE) values ('2','Algorithmique, M�thodologie et Programmation imp�rative');
Insert into CATEGORIES (NO_CAT,CATEGORIE) values ('3','M�thodologie, Conception et Programmation Objet');
Insert into CATEGORIES (NO_CAT,CATEGORIE) values ('4','Techniques Informatiques');
Insert into CATEGORIES (NO_CAT,CATEGORIE) values ('5','Bases de Donn�es');
Insert into CATEGORIES (NO_CAT,CATEGORIE) values ('6','D�couvertes Informatique Avanc�e');
Insert into CATEGORIES (NO_CAT,CATEGORIE) values ('7','D�couvertes autres disciplines');
Insert into CATEGORIES (NO_CAT,CATEGORIE) values ('8','Langue');
Insert into CATEGORIES (NO_CAT,CATEGORIE) values ('9','Projets/Stage');
Insert into CATEGORIES (NO_CAT,CATEGORIE) values ('10','Remise � niveau ou Parcours ECS');
Insert into CATEGORIES (NO_CAT,CATEGORIE) values ('11','D�veloppement Web et Mobile');
Insert into CATEGORIES (NO_CAT,CATEGORIE) values ('0','Divers');
Insert into CATEGORIES (NO_CAT,CATEGORIE) values ('12','Syst�me et R�seaux');
Insert into CATEGORIES (NO_CAT,CATEGORIE) values ('13','Mati�res transverses');
REM INSERTING into COMPOSANTE
SET DEFINE OFF;
Insert into COMPOSANTE (ID_COMP,NOM_COMP,LIEU_COMP) values ('FB0','UFR MIM','Metz Technop�le');
Insert into COMPOSANTE (ID_COMP,NOM_COMP,LIEU_COMP) values ('HE0','IUT Metz','Saulcy');
REM INSERTING into DIPLOME
SET DEFINE OFF;
Insert into DIPLOME (CODE_DIPLOME,LIBELLE_DIPLOME,VDI,LIBELLE_VDI,ANNEE_DEB,ANNEE_FIN) values ('3WLFPW1','Portail Math�matiques Informatique','300','Portail Math�matiques Informatique','2018','2023');
Insert into DIPLOME (CODE_DIPLOME,LIBELLE_DIPLOME,VDI,LIBELLE_VDI,ANNEE_DEB,ANNEE_FIN) values ('3WLFINF','S&T_Licence Informatique','200','L2-Informatique','2018','2023');
Insert into DIPLOME (CODE_DIPLOME,LIBELLE_DIPLOME,VDI,LIBELLE_VDI,ANNEE_DEB,ANNEE_FIN) values ('3WLFINF','S&T_Licence Informatique','300','L3-Informatique','2018','2023');
Insert into DIPLOME (CODE_DIPLOME,LIBELLE_DIPLOME,VDI,LIBELLE_VDI,ANNEE_DEB,ANNEE_FIN) values ('5WMFINF','S&T_Master Informatique','400','M1-Informatique','2018','2023');
Insert into DIPLOME (CODE_DIPLOME,LIBELLE_DIPLOME,VDI,LIBELLE_VDI,ANNEE_DEB,ANNEE_FIN) values ('5WMFINF','S&T_Master Informatique','502','M2-Informatique','2018','2023');
Insert into DIPLOME (CODE_DIPLOME,LIBELLE_DIPLOME,VDI,LIBELLE_VDI,ANNEE_DEB,ANNEE_FIN) values ('5WMFINF','S&T_Master Informatique','503','M2-Informatique','2018','2023');
Insert into DIPLOME (CODE_DIPLOME,LIBELLE_DIPLOME,VDI,LIBELLE_VDI,ANNEE_DEB,ANNEE_FIN) values ('5WMFINF','S&T_Master Informatique','504','M2-Informatique','2018','2023');
REM INSERTING into EC
SET DEFINE OFF;
Insert into EC (CODE_EC,LIBELLE_EC,NATURE_EC,HCM,HTD,HTP,HEI,HPRJ,HTPL,CNU,NO_CAT,CODE_EC_PERE,CODE_UE) values ('8WP31M01','Initiation � la Recherche','P',null,null,null,null,'96',null,null,'9',null,'8WU31M28');
Insert into EC (CODE_EC,LIBELLE_EC,NATURE_EC,HCM,HTD,HTP,HEI,HPRJ,HTPL,CNU,NO_CAT,CODE_EC_PERE,CODE_UE) values ('8WT31M01','Projet en Entreprise (Stage en Alternance)','S',null,null,null,null,null,null,null,'9',null,'8WU31M29');
Insert into EC (CODE_EC,LIBELLE_EC,NATURE_EC,HCM,HTD,HTP,HEI,HPRJ,HTPL,CNU,NO_CAT,CODE_EC_PERE,CODE_UE) values ('6WT31M01','Stage','S',null,null,null,null,null,null,null,'9',null,'6WU31M08');
Insert into EC (CODE_EC,LIBELLE_EC,NATURE_EC,HCM,HTD,HTP,HEI,HPRJ,HTPL,CNU,NO_CAT,CODE_EC_PERE,CODE_UE) values ('2WEK7M11','Algorithmique et Programmation 2','E',null,null,'20','40',null,null,'2700','3',null,'2WUK7M01');
Insert into EC (CODE_EC,LIBELLE_EC,NATURE_EC,HCM,HTD,HTP,HEI,HPRJ,HTPL,CNU,NO_CAT,CODE_EC_PERE,CODE_UE) values ('6WE31M13','Pr�paration � l''immersion professionnelle','E',null,'10',null,null,null,null,'2700','9',null,'6WU31M08');
Insert into EC (CODE_EC,LIBELLE_EC,NATURE_EC,HCM,HTD,HTP,HEI,HPRJ,HTPL,CNU,NO_CAT,CODE_EC_PERE,CODE_UE) values ('3WE31M09','Introduction aux Bases de Donn�es','E','10','12','8',null,null,null,'2700','5',null,'3WU31M07');
Insert into EC (CODE_EC,LIBELLE_EC,NATURE_EC,HCM,HTD,HTP,HEI,HPRJ,HTPL,CNU,NO_CAT,CODE_EC_PERE,CODE_UE) values ('9WEHMM31','Cr�ation d''entreprise (Alternance)','E','6','6',null,null,null,null,'7100','7',null,'9WUHMM51');
Insert into EC (CODE_EC,LIBELLE_EC,NATURE_EC,HCM,HTD,HTP,HEI,HPRJ,HTPL,CNU,NO_CAT,CODE_EC_PERE,CODE_UE) values ('1WEK7M14','Culture Scientifique','E',null,null,null,'30',null,null,'2700','7',null,'1WUK7M08');
Insert into EC (CODE_EC,LIBELLE_EC,NATURE_EC,HCM,HTD,HTP,HEI,HPRJ,HTPL,CNU,NO_CAT,CODE_EC_PERE,CODE_UE) values ('1WEK7M03','Syst�mes logiques et num�riques','E',null,null,null,'15',null,null,'6100','7',null,'1WUK7M09');
Insert into EC (CODE_EC,LIBELLE_EC,NATURE_EC,HCM,HTD,HTP,HEI,HPRJ,HTPL,CNU,NO_CAT,CODE_EC_PERE,CODE_UE) values ('1WEL2M01','Principes de macro�conomie (1)','E','30','15',null,null,null,null,'500','7',null,'1WUK7M11');
Insert into EC (CODE_EC,LIBELLE_EC,NATURE_EC,HCM,HTD,HTP,HEI,HPRJ,HTPL,CNU,NO_CAT,CODE_EC_PERE,CODE_UE) values ('1WEK7M16','Allemand','E',null,null,null,null,null,'20','1200','8','1WCK7M04','2WUK7M10');
Insert into EC (CODE_EC,LIBELLE_EC,NATURE_EC,HCM,HTD,HTP,HEI,HPRJ,HTPL,CNU,NO_CAT,CODE_EC_PERE,CODE_UE) values ('1WEK7M07','M�thodologie du Travail Universitaire','E',null,'10',null,null,null,null,'0','0',null,'1WUK7M12');
Insert into EC (CODE_EC,LIBELLE_EC,NATURE_EC,HCM,HTD,HTP,HEI,HPRJ,HTPL,CNU,NO_CAT,CODE_EC_PERE,CODE_UE) values ('2WEK7M20','Introduction aux syst�mes s�quentiels','E',null,null,'9','21',null,null,'6100','7',null,'2WUK7M16');
Insert into EC (CODE_EC,LIBELLE_EC,NATURE_EC,HCM,HTD,HTP,HEI,HPRJ,HTPL,CNU,NO_CAT,CODE_EC_PERE,CODE_UE) values ('2WEK7M10','Introduction a la statistique exploratoire','E',null,null,null,'20',null,null,'2600','1',null,'2WUK7M10');
Insert into EC (CODE_EC,LIBELLE_EC,NATURE_EC,HCM,HTD,HTP,HEI,HPRJ,HTPL,CNU,NO_CAT,CODE_EC_PERE,CODE_UE) values ('2WEK7M21','Circuits en r�gime transitoire','E',null,null,'12','18',null,null,'6300','7',null,'2WUK7M16');
Insert into EC (CODE_EC,LIBELLE_EC,NATURE_EC,HCM,HTD,HTP,HEI,HPRJ,HTPL,CNU,NO_CAT,CODE_EC_PERE,CODE_UE) values ('2WEK7M22','M�canique du solide','E',null,null,'16','44',null,null,'6000','7',null,'2WUK7M17');
Insert into EC (CODE_EC,LIBELLE_EC,NATURE_EC,HCM,HTD,HTP,HEI,HPRJ,HTPL,CNU,NO_CAT,CODE_EC_PERE,CODE_UE) values ('2WEK7M18','Compl�ments d''analyse','E',null,null,'4','26',null,null,'2600','1',null,'2WUK7M12');
Insert into EC (CODE_EC,LIBELLE_EC,NATURE_EC,HCM,HTD,HTP,HEI,HPRJ,HTPL,CNU,NO_CAT,CODE_EC_PERE,CODE_UE) values ('1WEK7M11','Introduction au Web','E',null,null,'22','8',null,null,'2700','11',null,'1WUK7M04');
Insert into EC (CODE_EC,LIBELLE_EC,NATURE_EC,HCM,HTD,HTP,HEI,HPRJ,HTPL,CNU,NO_CAT,CODE_EC_PERE,CODE_UE) values ('1WEK7M05','Anglais','E',null,null,null,null,null,'20','1100','8','1WCK7M04',null);
Insert into EC (CODE_EC,LIBELLE_EC,NATURE_EC,HCM,HTD,HTP,HEI,HPRJ,HTPL,CNU,NO_CAT,CODE_EC_PERE,CODE_UE) values ('1WEK7M06','Outils & Cultures NUM�riques','E','2',null,'18',null,null,null,'2700','1',null,'1WUK7M12');
Insert into EC (CODE_EC,LIBELLE_EC,NATURE_EC,HCM,HTD,HTP,HEI,HPRJ,HTPL,CNU,NO_CAT,CODE_EC_PERE,CODE_UE) values ('2WEK7M09','EC2 Ph�nom�nes �lectromagn�tiques','E',null,null,'9','21',null,null,'6300','7',null,'2WUK7M06');
Insert into EC (CODE_EC,LIBELLE_EC,NATURE_EC,HCM,HTD,HTP,HEI,HPRJ,HTPL,CNU,NO_CAT,CODE_EC_PERE,CODE_UE) values ('2WEK7M03','Outils & Cultures NUM�riques','E','2',null,'18',null,null,null,'2700','7',null,'2WUK7M04');
Insert into EC (CODE_EC,LIBELLE_EC,NATURE_EC,HCM,HTD,HTP,HEI,HPRJ,HTPL,CNU,NO_CAT,CODE_EC_PERE,CODE_UE) values ('2WEK7M16','Electromagn�tisme','E',null,null,null,'60',null,null,'6300','1',null,'2WUK7M09');
Insert into EC (CODE_EC,LIBELLE_EC,NATURE_EC,HCM,HTD,HTP,HEI,HPRJ,HTPL,CNU,NO_CAT,CODE_EC_PERE,CODE_UE) values ('1WEK7M13','Codage num�rique : du nombre au pixel','E',null,null,'6','24',null,null,'2700','4',null,'1WUK7M07');
Insert into EC (CODE_EC,LIBELLE_EC,NATURE_EC,HCM,HTD,HTP,HEI,HPRJ,HTPL,CNU,NO_CAT,CODE_EC_PERE,CODE_UE) values ('2WEK7M08','EC1 Intro aux syst�mes dynamiques et simulation num�rique','E',null,null,'9','21',null,null,'6100','7',null,'2WUK7M06');
Insert into EC (CODE_EC,LIBELLE_EC,NATURE_EC,HCM,HTD,HTP,HEI,HPRJ,HTPL,CNU,NO_CAT,CODE_EC_PERE,CODE_UE) values ('2WEK7M14','Alg�bre lin�aire 1','E',null,null,null,'60',null,null,'2500','1',null,'2WUK7M07');
Insert into EC (CODE_EC,LIBELLE_EC,NATURE_EC,HCM,HTD,HTP,HEI,HPRJ,HTPL,CNU,NO_CAT,CODE_EC_PERE,CODE_UE) values ('2WEK7M17','Arithm�tique','E',null,null,'4','26',null,null,'2500','1',null,'2WUK7M11');
Insert into EC (CODE_EC,LIBELLE_EC,NATURE_EC,HCM,HTD,HTP,HEI,HPRJ,HTPL,CNU,NO_CAT,CODE_EC_PERE,CODE_UE) values ('2WEK7M19','Codage Num�rique : du nombre au pixel','E',null,null,'6','24',null,null,'2700','4',null,'2WUK7M13');
Insert into EC (CODE_EC,LIBELLE_EC,NATURE_EC,HCM,HTD,HTP,HEI,HPRJ,HTPL,CNU,NO_CAT,CODE_EC_PERE,CODE_UE) values ('2WEK7M06','M�thodologie Niveau 1','E',null,null,'10','20',null,null,'2700','2',null,'2WUK7M14');
Insert into EC (CODE_EC,LIBELLE_EC,NATURE_EC,HCM,HTD,HTP,HEI,HPRJ,HTPL,CNU,NO_CAT,CODE_EC_PERE,CODE_UE) values ('1WEK7M15','M�canique du point','E',null,null,null,'30',null,null,'6000','7',null,'1WUK7M10');
Insert into EC (CODE_EC,LIBELLE_EC,NATURE_EC,HCM,HTD,HTP,HEI,HPRJ,HTPL,CNU,NO_CAT,CODE_EC_PERE,CODE_UE) values ('1WEK7M08','Calculs et math�matiques','E',null,null,null,'60',null,null,'2600','1',null,'1WUK7M01');
Insert into EC (CODE_EC,LIBELLE_EC,NATURE_EC,HCM,HTD,HTP,HEI,HPRJ,HTPL,CNU,NO_CAT,CODE_EC_PERE,CODE_UE) values ('1WEK7M10','Fondements math�matiques','E',null,null,null,'30',null,null,'2500','1',null,'1WUK7M03');
Insert into EC (CODE_EC,LIBELLE_EC,NATURE_EC,HCM,HTD,HTP,HEI,HPRJ,HTPL,CNU,NO_CAT,CODE_EC_PERE,CODE_UE) values ('2WEK7M13','Allemand','E',null,null,null,null,null,'20','1200','8','2WCK7M05',null);
Insert into EC (CODE_EC,LIBELLE_EC,NATURE_EC,HCM,HTD,HTP,HEI,HPRJ,HTPL,CNU,NO_CAT,CODE_EC_PERE,CODE_UE) values ('2WEK7M12','Outils Math�matiques','E',null,null,null,'60',null,null,'2600','1',null,'2WUK7M03');
Insert into EC (CODE_EC,LIBELLE_EC,NATURE_EC,HCM,HTD,HTP,HEI,HPRJ,HTPL,CNU,NO_CAT,CODE_EC_PERE,CODE_UE) values ('1WEK7M09','Algorithmique et Programmation 1','E',null,null,'16','44',null,null,'2700','2',null,'1WUK7M02');
Insert into EC (CODE_EC,LIBELLE_EC,NATURE_EC,HCM,HTD,HTP,HEI,HPRJ,HTPL,CNU,NO_CAT,CODE_EC_PERE,CODE_UE) values ('2WEK7M07','M�thodologie Niveau 2','E',null,null,'22','8',null,null,'2700','3',null,'2WUK7M15');
Insert into EC (CODE_EC,LIBELLE_EC,NATURE_EC,HCM,HTD,HTP,HEI,HPRJ,HTPL,CNU,NO_CAT,CODE_EC_PERE,CODE_UE) values ('2WEK7M04','Anglais','E',null,null,null,null,null,'20','1100','8','2WCK7M05',null);
Insert into EC (CODE_EC,LIBELLE_EC,NATURE_EC,HCM,HTD,HTP,HEI,HPRJ,HTPL,CNU,NO_CAT,CODE_EC_PERE,CODE_UE) values ('2WEK7M05','Projet professionnel personnel','E',null,'10',null,null,null,null,'0','0',null,'2WUK7M04');
Insert into EC (CODE_EC,LIBELLE_EC,NATURE_EC,HCM,HTD,HTP,HEI,HPRJ,HTPL,CNU,NO_CAT,CODE_EC_PERE,CODE_UE) values ('2WEL2M01','Principes de micro�conomie (1)','E','30','15',null,null,null,null,'500','7',null,'2WUK7M10');
Insert into EC (CODE_EC,LIBELLE_EC,NATURE_EC,HCM,HTD,HTP,HEI,HPRJ,HTPL,CNU,NO_CAT,CODE_EC_PERE,CODE_UE) values ('1WEK7M12','Nombres complexes et g�om�trie','E',null,null,null,'30',null,null,'2600','1',null,'1WUK7M06');
Insert into EC (CODE_EC,LIBELLE_EC,NATURE_EC,HCM,HTD,HTP,HEI,HPRJ,HTPL,CNU,NO_CAT,CODE_EC_PERE,CODE_UE) values ('2WEK7M15','Analyse 1','E',null,null,null,'60',null,null,'2600','1',null,'2WUK7M08');
Insert into EC (CODE_EC,LIBELLE_EC,NATURE_EC,HCM,HTD,HTP,HEI,HPRJ,HTPL,CNU,NO_CAT,CODE_EC_PERE,CODE_UE) values ('5WE31M11','Programmation fonctionnelle','E','14','16',null,null,null,null,'2700','2',null,'5WU31M05');
Insert into EC (CODE_EC,LIBELLE_EC,NATURE_EC,HCM,HTD,HTP,HEI,HPRJ,HTPL,CNU,NO_CAT,CODE_EC_PERE,CODE_UE) values ('5WE31M12','XML','E','10','8','12',null,null,null,'2700','4',null,'5WU31M06');
Insert into EC (CODE_EC,LIBELLE_EC,NATURE_EC,HCM,HTD,HTP,HEI,HPRJ,HTPL,CNU,NO_CAT,CODE_EC_PERE,CODE_UE) values ('5WE31M10','Logique','E','30','30',null,null,null,null,'2700','1',null,'5WU31M01');
Insert into EC (CODE_EC,LIBELLE_EC,NATURE_EC,HCM,HTD,HTP,HEI,HPRJ,HTPL,CNU,NO_CAT,CODE_EC_PERE,CODE_UE) values ('6WE31M08','Infographie','E','14',null,'16',null,null,null,'2700','6',null,'6WU31M06');
Insert into EC (CODE_EC,LIBELLE_EC,NATURE_EC,HCM,HTD,HTP,HEI,HPRJ,HTPL,CNU,NO_CAT,CODE_EC_PERE,CODE_UE) values ('6WE31M05','Compilation','E','14','16',null,null,null,null,'2700','4',null,'6WU31M03');
Insert into EC (CODE_EC,LIBELLE_EC,NATURE_EC,HCM,HTD,HTP,HEI,HPRJ,HTPL,CNU,NO_CAT,CODE_EC_PERE,CODE_UE) values ('5WE31M01','Algorithmique','E','25','20',null,null,null,null,'2700','2',null,'5WU31M02');
Insert into EC (CODE_EC,LIBELLE_EC,NATURE_EC,HCM,HTD,HTP,HEI,HPRJ,HTPL,CNU,NO_CAT,CODE_EC_PERE,CODE_UE) values ('5WE31M08','Anglais','E',null,null,null,null,null,'20','1100','8',null,'5WU31M09');
Insert into EC (CODE_EC,LIBELLE_EC,NATURE_EC,HCM,HTD,HTP,HEI,HPRJ,HTPL,CNU,NO_CAT,CODE_EC_PERE,CODE_UE) values ('5WE31M09','Projet Personnel professionnel','E',null,'20',null,null,null,null,'0','13',null,'5WU31M09');
Insert into EC (CODE_EC,LIBELLE_EC,NATURE_EC,HCM,HTD,HTP,HEI,HPRJ,HTPL,CNU,NO_CAT,CODE_EC_PERE,CODE_UE) values ('5WE31M06','Compression de donn�es','E','14','16',null,null,null,null,'2700','4',null,'5WU31M07');
Insert into EC (CODE_EC,LIBELLE_EC,NATURE_EC,HCM,HTD,HTP,HEI,HPRJ,HTPL,CNU,NO_CAT,CODE_EC_PERE,CODE_UE) values ('5WE31M07','Robotique','E','14',null,'16',null,null,null,'2700','7',null,'5WU31M08');
Insert into EC (CODE_EC,LIBELLE_EC,NATURE_EC,HCM,HTD,HTP,HEI,HPRJ,HTPL,CNU,NO_CAT,CODE_EC_PERE,CODE_UE) values ('6WE31M04','Mod�lisation','E','14','16',null,null,null,null,'2700','6',null,'6WU31M02');
Insert into EC (CODE_EC,LIBELLE_EC,NATURE_EC,HCM,HTD,HTP,HEI,HPRJ,HTPL,CNU,NO_CAT,CODE_EC_PERE,CODE_UE) values ('5WE31M02','Conception Programmation Objet Avanc�e','E','14','16','15',null,null,null,'2700','3',null,'5WU31M02');
Insert into EC (CODE_EC,LIBELLE_EC,NATURE_EC,HCM,HTD,HTP,HEI,HPRJ,HTPL,CNU,NO_CAT,CODE_EC_PERE,CODE_UE) values ('6WE31M07','Introduction � l''Intelligence Artificielle','E','10','10','10',null,null,null,'2700','6',null,'6WU31M05');
Insert into EC (CODE_EC,LIBELLE_EC,NATURE_EC,HCM,HTD,HTP,HEI,HPRJ,HTPL,CNU,NO_CAT,CODE_EC_PERE,CODE_UE) values ('6WE31M02','R�seau 2','E','12','6','12',null,null,null,'2700','12',null,'6WU31M01');
Insert into EC (CODE_EC,LIBELLE_EC,NATURE_EC,HCM,HTD,HTP,HEI,HPRJ,HTPL,CNU,NO_CAT,CODE_EC_PERE,CODE_UE) values ('5WE31M04','Programmation web','E','12','6','12',null,null,null,'2700','11',null,'5WU31M03');
Insert into EC (CODE_EC,LIBELLE_EC,NATURE_EC,HCM,HTD,HTP,HEI,HPRJ,HTPL,CNU,NO_CAT,CODE_EC_PERE,CODE_UE) values ('5WE31M03','Conception et manipulation de bases de donn�es','E','8','10','12',null,null,null,'2700','5',null,'5WU31M03');
Insert into EC (CODE_EC,LIBELLE_EC,NATURE_EC,HCM,HTD,HTP,HEI,HPRJ,HTPL,CNU,NO_CAT,CODE_EC_PERE,CODE_UE) values ('6WE31M03','Optimisation','E','14','16',null,null,null,null,'2700','6',null,'6WU31M02');
Insert into EC (CODE_EC,LIBELLE_EC,NATURE_EC,HCM,HTD,HTP,HEI,HPRJ,HTPL,CNU,NO_CAT,CODE_EC_PERE,CODE_UE) values ('6WE31M11','Maths pour le CAPES','E',null,null,null,'30',null,null,'2700','1','6WC31M02',null);
Insert into EC (CODE_EC,LIBELLE_EC,NATURE_EC,HCM,HTD,HTP,HEI,HPRJ,HTPL,CNU,NO_CAT,CODE_EC_PERE,CODE_UE) values ('6WE31M09','D�veloppement d''applications mobiles','E','10','8','12',null,null,null,'2700','11',null,'6WU31M07');
Insert into EC (CODE_EC,LIBELLE_EC,NATURE_EC,HCM,HTD,HTP,HEI,HPRJ,HTPL,CNU,NO_CAT,CODE_EC_PERE,CODE_UE) values ('6WE31M01','S�curit�','E','10','10','10',null,null,null,'2700','6',null,'6WU31M01');
Insert into EC (CODE_EC,LIBELLE_EC,NATURE_EC,HCM,HTD,HTP,HEI,HPRJ,HTPL,CNU,NO_CAT,CODE_EC_PERE,CODE_UE) values ('6WE31M12','Analyse de donn�es','E','16','14',null,null,null,null,'2700','6','6WC31M02',null);
Insert into EC (CODE_EC,LIBELLE_EC,NATURE_EC,HCM,HTD,HTP,HEI,HPRJ,HTPL,CNU,NO_CAT,CODE_EC_PERE,CODE_UE) values ('3WE31M01','Maths discr�tes 1','E','30','30',null,null,null,null,'2700','1',null,'3WU31M01');
Insert into EC (CODE_EC,LIBELLE_EC,NATURE_EC,HCM,HTD,HTP,HEI,HPRJ,HTPL,CNU,NO_CAT,CODE_EC_PERE,CODE_UE) values ('4WEULM04','Musique, arts et soci�t�','E','27',null,null,null,null,null,'1800','7',null,'4WU31M08');
Insert into EC (CODE_EC,LIBELLE_EC,NATURE_EC,HCM,HTD,HTP,HEI,HPRJ,HTPL,CNU,NO_CAT,CODE_EC_PERE,CODE_UE) values ('4WEULM06','Les fictions contemporaines','E','27',null,null,null,null,null,'1000','7',null,'4WU31M08');
Insert into EC (CODE_EC,LIBELLE_EC,NATURE_EC,HCM,HTD,HTP,HEI,HPRJ,HTPL,CNU,NO_CAT,CODE_EC_PERE,CODE_UE) values ('4WEULM09','Tutorat pour les cord�es de la r�ussite','E','27',null,null,null,null,null,'0','7',null,'4WU31M08');
Insert into EC (CODE_EC,LIBELLE_EC,NATURE_EC,HCM,HTD,HTP,HEI,HPRJ,HTPL,CNU,NO_CAT,CODE_EC_PERE,CODE_UE) values ('4WEULM19','Sociologie g�n�rale','E','27',null,null,null,null,null,'1900','7',null,'4WU31M08');
Insert into EC (CODE_EC,LIBELLE_EC,NATURE_EC,HCM,HTD,HTP,HEI,HPRJ,HTPL,CNU,NO_CAT,CODE_EC_PERE,CODE_UE) values ('4WEULM27','La Main � la p�te : animat� scient � l''�cole primaire','E','27',null,null,null,null,null,'0','7',null,'4WU31M08');
Insert into EC (CODE_EC,LIBELLE_EC,NATURE_EC,HCM,HTD,HTP,HEI,HPRJ,HTPL,CNU,NO_CAT,CODE_EC_PERE,CODE_UE) values ('4WEULM37','R�seau social, solidarit�s �tudiantes et vieillissement','E','27',null,null,null,null,null,'1600','7',null,'4WU31M08');
Insert into EC (CODE_EC,LIBELLE_EC,NATURE_EC,HCM,HTD,HTP,HEI,HPRJ,HTPL,CNU,NO_CAT,CODE_EC_PERE,CODE_UE) values ('3WE31M06','Architecture des ordinateurs','E','18','15','12',null,null,null,'2700','4',null,'3WU31M04');
Insert into EC (CODE_EC,LIBELLE_EC,NATURE_EC,HCM,HTD,HTP,HEI,HPRJ,HTPL,CNU,NO_CAT,CODE_EC_PERE,CODE_UE) values ('3WE31M08','Langue 2','E',null,'45',null,null,null,null,'1200','8',null,'3WU31M06');
Insert into EC (CODE_EC,LIBELLE_EC,NATURE_EC,HCM,HTD,HTP,HEI,HPRJ,HTPL,CNU,NO_CAT,CODE_EC_PERE,CODE_UE) values ('4WEULM17','Psychologie','E','27',null,null,null,null,null,'1600','7',null,'4WU31M08');
Insert into EC (CODE_EC,LIBELLE_EC,NATURE_EC,HCM,HTD,HTP,HEI,HPRJ,HTPL,CNU,NO_CAT,CODE_EC_PERE,CODE_UE) values ('4WEULM26','ABC de l''astronomie','E','27',null,null,null,null,null,'3400','7',null,'4WU31M08');
Insert into EC (CODE_EC,LIBELLE_EC,NATURE_EC,HCM,HTD,HTP,HEI,HPRJ,HTPL,CNU,NO_CAT,CODE_EC_PERE,CODE_UE) values ('4WEULM31','Education pour la sant� autour des addictions','E','27',null,null,null,null,null,'4600','7',null,'4WU31M08');
Insert into EC (CODE_EC,LIBELLE_EC,NATURE_EC,HCM,HTD,HTP,HEI,HPRJ,HTPL,CNU,NO_CAT,CODE_EC_PERE,CODE_UE) values ('4WEULM32','Les jeux vid�o, une industrie culturelle','E','27',null,null,null,null,null,'7100','7',null,'4WU31M08');
Insert into EC (CODE_EC,LIBELLE_EC,NATURE_EC,HCM,HTD,HTP,HEI,HPRJ,HTPL,CNU,NO_CAT,CODE_EC_PERE,CODE_UE) values ('4WEULM35','Pratique de l''argumentation orale','E','27',null,null,null,null,null,'100','7',null,'4WU31M08');
Insert into EC (CODE_EC,LIBELLE_EC,NATURE_EC,HCM,HTD,HTP,HEI,HPRJ,HTPL,CNU,NO_CAT,CODE_EC_PERE,CODE_UE) values ('4WEULM23','Pratiques associatives et citoyennes et Handicap','E','27',null,null,null,null,null,'0','7',null,'4WU31M08');
Insert into EC (CODE_EC,LIBELLE_EC,NATURE_EC,HCM,HTD,HTP,HEI,HPRJ,HTPL,CNU,NO_CAT,CODE_EC_PERE,CODE_UE) values ('4WEULM33','Culture et patrimoine','E','27',null,null,null,null,null,'2100','7',null,'4WU31M08');
Insert into EC (CODE_EC,LIBELLE_EC,NATURE_EC,HCM,HTD,HTP,HEI,HPRJ,HTPL,CNU,NO_CAT,CODE_EC_PERE,CODE_UE) values ('3WE31M02','Outils Syst�me','E','6','4','20',null,null,null,'2700','12',null,'3WU31M08');
Insert into EC (CODE_EC,LIBELLE_EC,NATURE_EC,HCM,HTD,HTP,HEI,HPRJ,HTPL,CNU,NO_CAT,CODE_EC_PERE,CODE_UE) values ('4WE31M08','Langues 2','E',null,'45',null,null,null,null,'1200','8',null,'4WU31M06');
Insert into EC (CODE_EC,LIBELLE_EC,NATURE_EC,HCM,HTD,HTP,HEI,HPRJ,HTPL,CNU,NO_CAT,CODE_EC_PERE,CODE_UE) values ('4WE31M04','Projet de synth�se','E','4','8','18',null,null,null,'2700','9',null,'4WU31M02');
Insert into EC (CODE_EC,LIBELLE_EC,NATURE_EC,HCM,HTD,HTP,HEI,HPRJ,HTPL,CNU,NO_CAT,CODE_EC_PERE,CODE_UE) values ('4WEULM12','El�ments de d�fense et de s�curit� nationales','E','27',null,null,null,null,null,'600','7',null,'4WU31M08');
Insert into EC (CODE_EC,LIBELLE_EC,NATURE_EC,HCM,HTD,HTP,HEI,HPRJ,HTPL,CNU,NO_CAT,CODE_EC_PERE,CODE_UE) values ('4WEULM34','Connaissance de soi et Gestion de sa vie physique','E','27',null,null,null,null,null,'0','7',null,'4WU31M08');
Insert into EC (CODE_EC,LIBELLE_EC,NATURE_EC,HCM,HTD,HTP,HEI,HPRJ,HTPL,CNU,NO_CAT,CODE_EC_PERE,CODE_UE) values ('4WE31M02','Langages formels','E','14','16',null,null,null,null,'2700','1',null,'4WU31M01');
Insert into EC (CODE_EC,LIBELLE_EC,NATURE_EC,HCM,HTD,HTP,HEI,HPRJ,HTPL,CNU,NO_CAT,CODE_EC_PERE,CODE_UE) values ('4WEULM22','L''Europe et l''Union Europ�enne','E','27',null,null,null,null,null,'2300','7',null,'4WU31M08');
Insert into EC (CODE_EC,LIBELLE_EC,NATURE_EC,HCM,HTD,HTP,HEI,HPRJ,HTPL,CNU,NO_CAT,CODE_EC_PERE,CODE_UE) values ('4WEULM25','Math�matique et soci�t�','E','27',null,null,null,null,null,'2500','7',null,'4WU31M08');
Insert into EC (CODE_EC,LIBELLE_EC,NATURE_EC,HCM,HTD,HTP,HEI,HPRJ,HTPL,CNU,NO_CAT,CODE_EC_PERE,CODE_UE) values ('3WE31M04','Algorithmique Programmation 3','E','24','24','12',null,null,null,'2700','2',null,'3WU31M02');
Insert into EC (CODE_EC,LIBELLE_EC,NATURE_EC,HCM,HTD,HTP,HEI,HPRJ,HTPL,CNU,NO_CAT,CODE_EC_PERE,CODE_UE) values ('4WE31M06','Probabilit�s Statistiques','E',null,'45',null,null,null,null,'2700','1',null,'4WU31M04');
Insert into EC (CODE_EC,LIBELLE_EC,NATURE_EC,HCM,HTD,HTP,HEI,HPRJ,HTPL,CNU,NO_CAT,CODE_EC_PERE,CODE_UE) values ('4WE31M01','Maths discr�tes 2','E','14','16',null,null,null,null,'2700','1',null,'4WU31M01');
Insert into EC (CODE_EC,LIBELLE_EC,NATURE_EC,HCM,HTD,HTP,HEI,HPRJ,HTPL,CNU,NO_CAT,CODE_EC_PERE,CODE_UE) values ('4WEULM03','Civilisation et culture des pays germanophones : mythes et .','E','27',null,null,null,null,null,'1200','7',null,'4WU31M08');
Insert into EC (CODE_EC,LIBELLE_EC,NATURE_EC,HCM,HTD,HTP,HEI,HPRJ,HTPL,CNU,NO_CAT,CODE_EC_PERE,CODE_UE) values ('4WEULM05','Ecriture inventive et cr�ation litt�raire','E','27',null,null,null,null,null,'900','7',null,'4WU31M08');
Insert into EC (CODE_EC,LIBELLE_EC,NATURE_EC,HCM,HTD,HTP,HEI,HPRJ,HTPL,CNU,NO_CAT,CODE_EC_PERE,CODE_UE) values ('4WEULM36','Histoire de la coupe du monde de football','E','27',null,null,null,null,null,'7400','7',null,'4WU31M08');
Insert into EC (CODE_EC,LIBELLE_EC,NATURE_EC,HCM,HTD,HTP,HEI,HPRJ,HTPL,CNU,NO_CAT,CODE_EC_PERE,CODE_UE) values ('4WE31M10','Anglais','E',null,null,null,null,null,'30','1100','8',null,'4WU31M09');
Insert into EC (CODE_EC,LIBELLE_EC,NATURE_EC,HCM,HTD,HTP,HEI,HPRJ,HTPL,CNU,NO_CAT,CODE_EC_PERE,CODE_UE) values ('3WE31M07','Remise � niveau informatique','E',null,'45',null,null,null,null,'2700','10',null,'3WU31M05');
Insert into EC (CODE_EC,LIBELLE_EC,NATURE_EC,HCM,HTD,HTP,HEI,HPRJ,HTPL,CNU,NO_CAT,CODE_EC_PERE,CODE_UE) values ('3WE31M05','Programmation Avanc�e','E','10',null,'20',null,null,null,'2700','2',null,'3WU31M03');
Insert into EC (CODE_EC,LIBELLE_EC,NATURE_EC,HCM,HTD,HTP,HEI,HPRJ,HTPL,CNU,NO_CAT,CODE_EC_PERE,CODE_UE) values ('4WE31M05','Syst�me 1','E','12','6','12',null,null,null,'2700','12',null,'4WU31M03');
Insert into EC (CODE_EC,LIBELLE_EC,NATURE_EC,HCM,HTD,HTP,HEI,HPRJ,HTPL,CNU,NO_CAT,CODE_EC_PERE,CODE_UE) values ('4WEULM08','Probl�mes �conomiques et sociaux contemporains','E','27',null,null,null,null,null,'500','7',null,'4WU31M08');
Insert into EC (CODE_EC,LIBELLE_EC,NATURE_EC,HCM,HTD,HTP,HEI,HPRJ,HTPL,CNU,NO_CAT,CODE_EC_PERE,CODE_UE) values ('4WEULM13','Entreprendre, c''est facile !','E','27',null,null,null,null,null,'600','7',null,'4WU31M08');
Insert into EC (CODE_EC,LIBELLE_EC,NATURE_EC,HCM,HTD,HTP,HEI,HPRJ,HTPL,CNU,NO_CAT,CODE_EC_PERE,CODE_UE) values ('3WE31M03','R�seau 1','E','12','8','10',null,null,null,'2700','12',null,'3WU31M08');
Insert into EC (CODE_EC,LIBELLE_EC,NATURE_EC,HCM,HTD,HTP,HEI,HPRJ,HTPL,CNU,NO_CAT,CODE_EC_PERE,CODE_UE) values ('3WE31M10','Anglais','E',null,null,null,null,null,'30','1100','8',null,'3WU31M09');
Insert into EC (CODE_EC,LIBELLE_EC,NATURE_EC,HCM,HTD,HTP,HEI,HPRJ,HTPL,CNU,NO_CAT,CODE_EC_PERE,CODE_UE) values ('4WE31M09','Bases de la programmation objet','E','16','16','28',null,null,null,'2700','3',null,'4WU31M07');
Insert into EC (CODE_EC,LIBELLE_EC,NATURE_EC,HCM,HTD,HTP,HEI,HPRJ,HTPL,CNU,NO_CAT,CODE_EC_PERE,CODE_UE) values ('4WEULM07','Droit public pour les concours administratifs','E','27',null,null,null,null,null,'200','7',null,'3WU31M08');
Insert into EC (CODE_EC,LIBELLE_EC,NATURE_EC,HCM,HTD,HTP,HEI,HPRJ,HTPL,CNU,NO_CAT,CODE_EC_PERE,CODE_UE) values ('4WEULM10','Nutrition et sant�','E','27',null,null,null,null,null,'0','7',null,'3WU31M08');
Insert into EC (CODE_EC,LIBELLE_EC,NATURE_EC,HCM,HTD,HTP,HEI,HPRJ,HTPL,CNU,NO_CAT,CODE_EC_PERE,CODE_UE) values ('4WEULM20','Th�ologie : Religions & cin�ma','E','27',null,null,null,null,null,'7600','7',null,'3WU31M08');
Insert into EC (CODE_EC,LIBELLE_EC,NATURE_EC,HCM,HTD,HTP,HEI,HPRJ,HTPL,CNU,NO_CAT,CODE_EC_PERE,CODE_UE) values ('9WEHMM14','Anglais','E',null,null,null,null,null,'24','1100','8',null,null);
Insert into EC (CODE_EC,LIBELLE_EC,NATURE_EC,HCM,HTD,HTP,HEI,HPRJ,HTPL,CNU,NO_CAT,CODE_EC_PERE,CODE_UE) values ('9WECPM19','Marketing','E','40',null,null,null,null,null,'0','7',null,'9WUCPM20');
Insert into EC (CODE_EC,LIBELLE_EC,NATURE_EC,HCM,HTD,HTP,HEI,HPRJ,HTPL,CNU,NO_CAT,CODE_EC_PERE,CODE_UE) values ('0WEHMM03','D�couverte de l''IHM dans des domaines d''application','E','14','14',null,null,null,null,'2700','6',null,null);
Insert into EC (CODE_EC,LIBELLE_EC,NATURE_EC,HCM,HTD,HTP,HEI,HPRJ,HTPL,CNU,NO_CAT,CODE_EC_PERE,CODE_UE) values ('9WEHMM03','980 Entrep�t de Donn�es','E','12','12',null,null,null,null,'2700','6',null,null);
Insert into EC (CODE_EC,LIBELLE_EC,NATURE_EC,HCM,HTD,HTP,HEI,HPRJ,HTPL,CNU,NO_CAT,CODE_EC_PERE,CODE_UE) values ('9WEHMM17','D�veloppement mobile','E','24','24',null,null,null,null,'2700','11',null,null);
Insert into EC (CODE_EC,LIBELLE_EC,NATURE_EC,HCM,HTD,HTP,HEI,HPRJ,HTPL,CNU,NO_CAT,CODE_EC_PERE,CODE_UE) values ('9WEHMM08','Ergonomie et facteurs humains pour l''accessibilt�','E','12','12',null,null,null,null,'1600','6',null,null);
Insert into EC (CODE_EC,LIBELLE_EC,NATURE_EC,HCM,HTD,HTP,HEI,HPRJ,HTPL,CNU,NO_CAT,CODE_EC_PERE,CODE_UE) values ('9WECPM18','Cr�ation d''entreprise','E','40',null,null,null,null,null,'0','7',null,'9WUCPM19');
Insert into EC (CODE_EC,LIBELLE_EC,NATURE_EC,HCM,HTD,HTP,HEI,HPRJ,HTPL,CNU,NO_CAT,CODE_EC_PERE,CODE_UE) values ('9WEHMM01','980 Conception Logicielle','E','12','12',null,null,null,null,'2700','6',null,null);
Insert into EC (CODE_EC,LIBELLE_EC,NATURE_EC,HCM,HTD,HTP,HEI,HPRJ,HTPL,CNU,NO_CAT,CODE_EC_PERE,CODE_UE) values ('9WEHMM05','939 S�curit� des R�seaux','E','12','12',null,null,null,null,'2700','12',null,null);
Insert into EC (CODE_EC,LIBELLE_EC,NATURE_EC,HCM,HTD,HTP,HEI,HPRJ,HTPL,CNU,NO_CAT,CODE_EC_PERE,CODE_UE) values ('9WEHMM20','ERp - Introduction au progiciel de gestion int�gr�','E','24','24',null,null,null,null,'2700','6',null,null);
Insert into EC (CODE_EC,LIBELLE_EC,NATURE_EC,HCM,HTD,HTP,HEI,HPRJ,HTPL,CNU,NO_CAT,CODE_EC_PERE,CODE_UE) values ('9WEHMM07','Recherche d''emploi et cr�ation d'' entreprise','E','12','12',null,null,null,null,'7100','7',null,null);
Insert into EC (CODE_EC,LIBELLE_EC,NATURE_EC,HCM,HTD,HTP,HEI,HPRJ,HTPL,CNU,NO_CAT,CODE_EC_PERE,CODE_UE) values ('9WEHMM12','Mod�les pour concevoir des syst�mes interactifs','E','12','12',null,null,null,null,'2700','6',null,'9WUHMM08');
Insert into EC (CODE_EC,LIBELLE_EC,NATURE_EC,HCM,HTD,HTP,HEI,HPRJ,HTPL,CNU,NO_CAT,CODE_EC_PERE,CODE_UE) values ('9WEHMM06','932 S�curit� des Syst�mes d''Information','E','12','12',null,null,null,null,'2700','6',null,null);
Insert into EC (CODE_EC,LIBELLE_EC,NATURE_EC,HCM,HTD,HTP,HEI,HPRJ,HTPL,CNU,NO_CAT,CODE_EC_PERE,CODE_UE) values ('0WEHMM04','EC Compl�ments G�nie Logiciel','E','14','14',null,null,null,null,'2700','6',null,null);
Insert into EC (CODE_EC,LIBELLE_EC,NATURE_EC,HCM,HTD,HTP,HEI,HPRJ,HTPL,CNU,NO_CAT,CODE_EC_PERE,CODE_UE) values ('9WEHMM29','Anglais','E',null,null,null,null,null,'24','1100','8',null,null);
Insert into EC (CODE_EC,LIBELLE_EC,NATURE_EC,HCM,HTD,HTP,HEI,HPRJ,HTPL,CNU,NO_CAT,CODE_EC_PERE,CODE_UE) values ('9WEJAM31','Enseignement Interculturel','E','26','4',null,null,null,null,'1600','7',null,null);
Insert into EC (CODE_EC,LIBELLE_EC,NATURE_EC,HCM,HTD,HTP,HEI,HPRJ,HTPL,CNU,NO_CAT,CODE_EC_PERE,CODE_UE) values ('9WEHMM09','M�thodes pour la conception centr�e utilisateurs','E','12','12',null,null,null,null,'1600','6',null,'9WUHMM05');
Insert into EC (CODE_EC,LIBELLE_EC,NATURE_EC,HCM,HTD,HTP,HEI,HPRJ,HTPL,CNU,NO_CAT,CODE_EC_PERE,CODE_UE) values ('9WEHMM11','Techniques de Visualisation de donn�es et analyse','E','12','12',null,null,null,null,'2700','6',null,'9WUHMM07');
Insert into EC (CODE_EC,LIBELLE_EC,NATURE_EC,HCM,HTD,HTP,HEI,HPRJ,HTPL,CNU,NO_CAT,CODE_EC_PERE,CODE_UE) values ('9WEHMM16','Techniques d''interactions innovantes','E','24','24',null,null,null,null,'2700','6',null,null);
Insert into EC (CODE_EC,LIBELLE_EC,NATURE_EC,HCM,HTD,HTP,HEI,HPRJ,HTPL,CNU,NO_CAT,CODE_EC_PERE,CODE_UE) values ('9WEIMM33','Optimisation pour l''industrie','E','12','12',null,null,null,null,'2700','6',null,'9WUIMM33');
Insert into EC (CODE_EC,LIBELLE_EC,NATURE_EC,HCM,HTD,HTP,HEI,HPRJ,HTPL,CNU,NO_CAT,CODE_EC_PERE,CODE_UE) values ('9WEIMM08','Th�orie de la complexit� et inapproximabilit�','E','24',null,null,null,null,null,'2700','6',null,'9WUIMM08');
Insert into EC (CODE_EC,LIBELLE_EC,NATURE_EC,HCM,HTD,HTP,HEI,HPRJ,HTPL,CNU,NO_CAT,CODE_EC_PERE,CODE_UE) values ('9WEIMM09','Int�gration m�thodologique SID','E',null,'36',null,null,null,null,'2700','6',null,'9WUIMM09');
Insert into EC (CODE_EC,LIBELLE_EC,NATURE_EC,HCM,HTD,HTP,HEI,HPRJ,HTPL,CNU,NO_CAT,CODE_EC_PERE,CODE_UE) values ('9WEIMM16','Portail web d''entreprise 1','E','12','12',null,null,null,null,'2700','11',null,null);
Insert into EC (CODE_EC,LIBELLE_EC,NATURE_EC,HCM,HTD,HTP,HEI,HPRJ,HTPL,CNU,NO_CAT,CODE_EC_PERE,CODE_UE) values ('9WEIMM32','Portails web d''entreprises 2','E','12','12',null,null,null,null,'2700','11',null,'9WUIMM32');
Insert into EC (CODE_EC,LIBELLE_EC,NATURE_EC,HCM,HTD,HTP,HEI,HPRJ,HTPL,CNU,NO_CAT,CODE_EC_PERE,CODE_UE) values ('9WEIMM14','Conception d''un syst�me d''information d�cisionnel','E','12','12',null,null,null,null,'2700','6',null,'9WUIMM15');
Insert into EC (CODE_EC,LIBELLE_EC,NATURE_EC,HCM,HTD,HTP,HEI,HPRJ,HTPL,CNU,NO_CAT,CODE_EC_PERE,CODE_UE) values ('0WEIMM01','Applications de l''informatique d�cisionnelle','E','14','14',null,null,null,null,'2700','6',null,null);
Insert into EC (CODE_EC,LIBELLE_EC,NATURE_EC,HCM,HTD,HTP,HEI,HPRJ,HTPL,CNU,NO_CAT,CODE_EC_PERE,CODE_UE) values ('9WEIMM19','D�cision pour les syst�mes parall�les et distribu�s','E','12','12',null,null,null,null,'2700','6',null,'9WUIMM20');
Insert into EC (CODE_EC,LIBELLE_EC,NATURE_EC,HCM,HTD,HTP,HEI,HPRJ,HTPL,CNU,NO_CAT,CODE_EC_PERE,CODE_UE) values ('9WEIMM06','Th�orie des graphes','E','24',null,null,null,null,null,'2700','6',null,'9WUIMM06');
Insert into EC (CODE_EC,LIBELLE_EC,NATURE_EC,HCM,HTD,HTP,HEI,HPRJ,HTPL,CNU,NO_CAT,CODE_EC_PERE,CODE_UE) values ('9WEIMM11','Professionnalisation SID','E','12','12',null,null,null,null,'2700','6',null,'9WUIMM11');
Insert into EC (CODE_EC,LIBELLE_EC,NATURE_EC,HCM,HTD,HTP,HEI,HPRJ,HTPL,CNU,NO_CAT,CODE_EC_PERE,CODE_UE) values ('9WEIMM15','Reporting&Dashboarding: cr�ation de tableaux de bord','E','12','12',null,null,null,null,'2700','6',null,null);
Insert into EC (CODE_EC,LIBELLE_EC,NATURE_EC,HCM,HTD,HTP,HEI,HPRJ,HTPL,CNU,NO_CAT,CODE_EC_PERE,CODE_UE) values ('9WEIMM31','Mod�les d�cisionnels pour la s�curit� des SI','E','12','12',null,null,null,null,'2700','6',null,null);
Insert into EC (CODE_EC,LIBELLE_EC,NATURE_EC,HCM,HTD,HTP,HEI,HPRJ,HTPL,CNU,NO_CAT,CODE_EC_PERE,CODE_UE) values ('9WEIMM10','Gestion de projets','E','12','12',null,null,null,null,'2700','13',null,null);
Insert into EC (CODE_EC,LIBELLE_EC,NATURE_EC,HCM,HTD,HTP,HEI,HPRJ,HTPL,CNU,NO_CAT,CODE_EC_PERE,CODE_UE) values ('9WEIMM12','Datawarehouse','E','12','12',null,null,null,null,'2700','6',null,'9WUIMM13');
Insert into EC (CODE_EC,LIBELLE_EC,NATURE_EC,HCM,HTD,HTP,HEI,HPRJ,HTPL,CNU,NO_CAT,CODE_EC_PERE,CODE_UE) values ('9WEIMM21','Mod�les de satisfaisabilit� - SAT','E','12','12',null,null,null,null,'2700','6',null,null);
Insert into EC (CODE_EC,LIBELLE_EC,NATURE_EC,HCM,HTD,HTP,HEI,HPRJ,HTPL,CNU,NO_CAT,CODE_EC_PERE,CODE_UE) values ('9WEIMM23','Fouille informatique des donn�es','E','12','12',null,null,null,null,'2700','6',null,'9WUIMM24');
Insert into EC (CODE_EC,LIBELLE_EC,NATURE_EC,HCM,HTD,HTP,HEI,HPRJ,HTPL,CNU,NO_CAT,CODE_EC_PERE,CODE_UE) values ('9WEIMM24','Techniques de visualisation de donn�es et analyse','E','12','12',null,null,null,null,null,'6',null,'9WUIMM25');
Insert into EC (CODE_EC,LIBELLE_EC,NATURE_EC,HCM,HTD,HTP,HEI,HPRJ,HTPL,CNU,NO_CAT,CODE_EC_PERE,CODE_UE) values ('9WEIMM27','Bioinformatique et datamining','E','12','12',null,null,null,null,'2700','6',null,'9WUIMM27');
Insert into EC (CODE_EC,LIBELLE_EC,NATURE_EC,HCM,HTD,HTP,HEI,HPRJ,HTPL,CNU,NO_CAT,CODE_EC_PERE,CODE_UE) values ('9WEIMM02','Gestion de projets','E','12','12',null,null,null,null,'2700','13',null,null);
Insert into EC (CODE_EC,LIBELLE_EC,NATURE_EC,HCM,HTD,HTP,HEI,HPRJ,HTPL,CNU,NO_CAT,CODE_EC_PERE,CODE_UE) values ('9WEIMM25','Algorithmique d'' analyse de donn�es','E','12','12',null,null,null,null,'2700','2',null,null);
Insert into EC (CODE_EC,LIBELLE_EC,NATURE_EC,HCM,HTD,HTP,HEI,HPRJ,HTPL,CNU,NO_CAT,CODE_EC_PERE,CODE_UE) values ('9WEIMM29','Optimisation et logiciels - Etude de cas en transport','E','12','12',null,null,null,null,'2700','6',null,'9WUIMM29');
Insert into EC (CODE_EC,LIBELLE_EC,NATURE_EC,HCM,HTD,HTP,HEI,HPRJ,HTPL,CNU,NO_CAT,CODE_EC_PERE,CODE_UE) values ('9WEIMM01','Int�gration m�thodologique OPAL','E',null,'36',null,null,null,null,'2700','6',null,'9WUIMM01');
Insert into EC (CODE_EC,LIBELLE_EC,NATURE_EC,HCM,HTD,HTP,HEI,HPRJ,HTPL,CNU,NO_CAT,CODE_EC_PERE,CODE_UE) values ('9WEIMM05','Combinatoire et algorithmes d''approximation','E','24',null,null,null,null,null,'2700','2',null,'9WUIMM05');
Insert into EC (CODE_EC,LIBELLE_EC,NATURE_EC,HCM,HTD,HTP,HEI,HPRJ,HTPL,CNU,NO_CAT,CODE_EC_PERE,CODE_UE) values ('9WEIMM22','Big Data','E','12','12',null,null,null,null,'2700','5',null,null);
Insert into EC (CODE_EC,LIBELLE_EC,NATURE_EC,HCM,HTD,HTP,HEI,HPRJ,HTPL,CNU,NO_CAT,CODE_EC_PERE,CODE_UE) values ('9WEIMM28','Ordonnancement et application','E','12','12',null,null,null,null,'2700','6',null,'9WUIMM28');
Insert into EC (CODE_EC,LIBELLE_EC,NATURE_EC,HCM,HTD,HTP,HEI,HPRJ,HTPL,CNU,NO_CAT,CODE_EC_PERE,CODE_UE) values ('9WEIMM03','Professionnalisation OPAL','E','12','12',null,null,null,null,'2700','13',null,'9WUIMM03');
Insert into EC (CODE_EC,LIBELLE_EC,NATURE_EC,HCM,HTD,HTP,HEI,HPRJ,HTPL,CNU,NO_CAT,CODE_EC_PERE,CODE_UE) values ('9WEIMM04','R�solution de mod�les d''optimisation de grande taille','E','24',null,null,null,null,null,'2700','6',null,'9WUIMM04');
Insert into EC (CODE_EC,LIBELLE_EC,NATURE_EC,HCM,HTD,HTP,HEI,HPRJ,HTPL,CNU,NO_CAT,CODE_EC_PERE,CODE_UE) values ('9WEIMM18','D�cision dans l''incertain','E','12','12',null,null,null,null,'2700','6',null,'9WUIMM19');
Insert into EC (CODE_EC,LIBELLE_EC,NATURE_EC,HCM,HTD,HTP,HEI,HPRJ,HTPL,CNU,NO_CAT,CODE_EC_PERE,CODE_UE) values ('9WEIMM07','Optimisation globale','E','24',null,null,null,null,null,'2700','6',null,'9WUIMM07');
Insert into EC (CODE_EC,LIBELLE_EC,NATURE_EC,HCM,HTD,HTP,HEI,HPRJ,HTPL,CNU,NO_CAT,CODE_EC_PERE,CODE_UE) values ('9WEIMM13','S�curit� des syst�mes d''information','E','12','12',null,null,null,null,'2700','6',null,'9WUIMM14');
Insert into EC (CODE_EC,LIBELLE_EC,NATURE_EC,HCM,HTD,HTP,HEI,HPRJ,HTPL,CNU,NO_CAT,CODE_EC_PERE,CODE_UE) values ('9WEIMM17','Aide � la d�cision multicrit�re','E','12','12',null,null,null,null,'2700','6',null,'9WUIMM18');
Insert into EC (CODE_EC,LIBELLE_EC,NATURE_EC,HCM,HTD,HTP,HEI,HPRJ,HTPL,CNU,NO_CAT,CODE_EC_PERE,CODE_UE) values ('9WEIMM20','Probl�mes de satisfaction de contraintes','E','12','12',null,null,null,null,'2700','6',null,'9WUIMM21');
Insert into EC (CODE_EC,LIBELLE_EC,NATURE_EC,HCM,HTD,HTP,HEI,HPRJ,HTPL,CNU,NO_CAT,CODE_EC_PERE,CODE_UE) values ('9WEOTM14','Contr�le d''acc�s','E','12','12',null,null,null,null,'2700','6',null,'9WUOTM14');
Insert into EC (CODE_EC,LIBELLE_EC,NATURE_EC,HCM,HTD,HTP,HEI,HPRJ,HTPL,CNU,NO_CAT,CODE_EC_PERE,CODE_UE) values ('9WEOTM06','Audit S�curit� d''un Syst�me d''Information','E','12','12',null,null,null,null,'2700','6',null,null);
Insert into EC (CODE_EC,LIBELLE_EC,NATURE_EC,HCM,HTD,HTP,HEI,HPRJ,HTPL,CNU,NO_CAT,CODE_EC_PERE,CODE_UE) values ('9WEOTM08','R�silience des Syst�mes d''information','E','12','12',null,null,null,null,'2700','6',null,'9WUOTM08');
Insert into EC (CODE_EC,LIBELLE_EC,NATURE_EC,HCM,HTD,HTP,HEI,HPRJ,HTPL,CNU,NO_CAT,CODE_EC_PERE,CODE_UE) values ('9WEOTM10','Malware, r�tro-ing�ni�rie','E','12','12',null,null,null,null,'2700','6',null,null);
Insert into EC (CODE_EC,LIBELLE_EC,NATURE_EC,HCM,HTD,HTP,HEI,HPRJ,HTPL,CNU,NO_CAT,CODE_EC_PERE,CODE_UE) values ('9WEOTM13','S�curit� des syst�mes','E','12','12',null,null,null,null,'2700','12',null,null);
Insert into EC (CODE_EC,LIBELLE_EC,NATURE_EC,HCM,HTD,HTP,HEI,HPRJ,HTPL,CNU,NO_CAT,CODE_EC_PERE,CODE_UE) values ('9WEOTM02','Gestion de projet','E','12','12',null,null,null,null,'2700','13',null,'9WUOTM02');
Insert into EC (CODE_EC,LIBELLE_EC,NATURE_EC,HCM,HTD,HTP,HEI,HPRJ,HTPL,CNU,NO_CAT,CODE_EC_PERE,CODE_UE) values ('9WEOTM12','S�curit� des Donn�es','E','12','12',null,null,null,null,'2700','6',null,'9WUOTM12');
Insert into EC (CODE_EC,LIBELLE_EC,NATURE_EC,HCM,HTD,HTP,HEI,HPRJ,HTPL,CNU,NO_CAT,CODE_EC_PERE,CODE_UE) values ('9WEOTM07','Identity Access Manag','E','12','12',null,null,null,null,'2700','6',null,'9WUOTM07');
Insert into EC (CODE_EC,LIBELLE_EC,NATURE_EC,HCM,HTD,HTP,HEI,HPRJ,HTPL,CNU,NO_CAT,CODE_EC_PERE,CODE_UE) values ('9WEOTM01','Int�gration m�thodologiques SSI','E',null,'36',null,null,null,null,'2700','6',null,'9WUOTM01');
Insert into EC (CODE_EC,LIBELLE_EC,NATURE_EC,HCM,HTD,HTP,HEI,HPRJ,HTPL,CNU,NO_CAT,CODE_EC_PERE,CODE_UE) values ('9WEOTM09','Tests d''intrusion','E','12','12',null,null,null,null,'2700','6',null,'9WUOTM09');
Insert into EC (CODE_EC,LIBELLE_EC,NATURE_EC,HCM,HTD,HTP,HEI,HPRJ,HTPL,CNU,NO_CAT,CODE_EC_PERE,CODE_UE) values ('9WEOTM11','S�curit� des r�seaux','E','12','12',null,null,null,null,'2700','12',null,'9WUOTM11');
Insert into EC (CODE_EC,LIBELLE_EC,NATURE_EC,HCM,HTD,HTP,HEI,HPRJ,HTPL,CNU,NO_CAT,CODE_EC_PERE,CODE_UE) values ('9WEOTM03','Recherche d''emploi et cr�ation d''entreprise','E','12','12',null,null,null,null,'2700','7',null,'9WUOTM03');
Insert into EC (CODE_EC,LIBELLE_EC,NATURE_EC,HCM,HTD,HTP,HEI,HPRJ,HTPL,CNU,NO_CAT,CODE_EC_PERE,CODE_UE) values ('9WEOTM04','S�curit� des Syst�mes d''information','E','12','12',null,null,null,null,'2700','6',null,'9WUOTM04');
Insert into EC (CODE_EC,LIBELLE_EC,NATURE_EC,HCM,HTD,HTP,HEI,HPRJ,HTPL,CNU,NO_CAT,CODE_EC_PERE,CODE_UE) values ('9WEOTM05','Politique de la S�curit� d''un Syst�mes d''Information','E','12','12',null,null,null,null,'2700','6',null,null);
Insert into EC (CODE_EC,LIBELLE_EC,NATURE_EC,HCM,HTD,HTP,HEI,HPRJ,HTPL,CNU,NO_CAT,CODE_EC_PERE,CODE_UE) values ('8WE31M01','Intelligence Artificielle','E','12','6','6',null,null,null,'2700','6',null,'8WU31M01');
Insert into EC (CODE_EC,LIBELLE_EC,NATURE_EC,HCM,HTD,HTP,HEI,HPRJ,HTPL,CNU,NO_CAT,CODE_EC_PERE,CODE_UE) values ('8WE31M11','Graphes d''attaques et R�seaux de transport','E','12','12',null,null,null,null,'2700','6',null,'8WU31M11');
Insert into EC (CODE_EC,LIBELLE_EC,NATURE_EC,HCM,HTD,HTP,HEI,HPRJ,HTPL,CNU,NO_CAT,CODE_EC_PERE,CODE_UE) values ('8WE31M24','Traitement d''images et vision par ordinateur','E','12','12',null,null,null,null,'2700','6',null,'8WU31M12');
Insert into EC (CODE_EC,LIBELLE_EC,NATURE_EC,HCM,HTD,HTP,HEI,HPRJ,HTPL,CNU,NO_CAT,CODE_EC_PERE,CODE_UE) values ('8WE31M02','Repr�sentation des donn�es visuelles','E','12',null,'12',null,null,null,'2700','6',null,'8WU31M02');
Insert into EC (CODE_EC,LIBELLE_EC,NATURE_EC,HCM,HTD,HTP,HEI,HPRJ,HTPL,CNU,NO_CAT,CODE_EC_PERE,CODE_UE) values ('7WE31M01','Analyse et conception de logiciels','E','22','6','16',null,null,null,'2700','6',null,'7WU31M01');
Insert into EC (CODE_EC,LIBELLE_EC,NATURE_EC,HCM,HTD,HTP,HEI,HPRJ,HTPL,CNU,NO_CAT,CODE_EC_PERE,CODE_UE) values ('7WE31M04','Logique et mod�les de calcul','E','22','22',null,null,null,null,'2700','6',null,'7WU31M04');
Insert into EC (CODE_EC,LIBELLE_EC,NATURE_EC,HCM,HTD,HTP,HEI,HPRJ,HTPL,CNU,NO_CAT,CODE_EC_PERE,CODE_UE) values ('7WE31M07','Anglais','E',null,null,null,null,null,'24','1100','8',null,'7WU31M07');
Insert into EC (CODE_EC,LIBELLE_EC,NATURE_EC,HCM,HTD,HTP,HEI,HPRJ,HTPL,CNU,NO_CAT,CODE_EC_PERE,CODE_UE) values ('8WE31M05','Outils d''aide � la d�cision','E','12','12',null,null,null,null,'2700','6',null,'8WU31M05');
Insert into EC (CODE_EC,LIBELLE_EC,NATURE_EC,HCM,HTD,HTP,HEI,HPRJ,HTPL,CNU,NO_CAT,CODE_EC_PERE,CODE_UE) values ('8WE31M23','Introduction � la s�curit� des syst�mes d''information','E','12','12',null,null,null,null,'2700','6',null,'8WU31M17');
Insert into EC (CODE_EC,LIBELLE_EC,NATURE_EC,HCM,HTD,HTP,HEI,HPRJ,HTPL,CNU,NO_CAT,CODE_EC_PERE,CODE_UE) values ('8WE31M18','Administration d''un syst�me en r�seau','E','12','12',null,null,null,null,'2700','12',null,'8WU31M18');
Insert into EC (CODE_EC,LIBELLE_EC,NATURE_EC,HCM,HTD,HTP,HEI,HPRJ,HTPL,CNU,NO_CAT,CODE_EC_PERE,CODE_UE) values ('8WE31M20','Ordonnancement temps-r�el sous Linux','E','12','12',null,null,null,null,'2700','12',null,'8WU31M20');
Insert into EC (CODE_EC,LIBELLE_EC,NATURE_EC,HCM,HTD,HTP,HEI,HPRJ,HTPL,CNU,NO_CAT,CODE_EC_PERE,CODE_UE) values ('8WE31M12','Ergonomie des syst�mes interactifs exp�rience utilisateur','E','12','12',null,null,null,null,'1600','6',null,'8WU31M22');
Insert into EC (CODE_EC,LIBELLE_EC,NATURE_EC,HCM,HTD,HTP,HEI,HPRJ,HTPL,CNU,NO_CAT,CODE_EC_PERE,CODE_UE) values ('8WE31M13','Psychologie cognitive et diversit� et interactions','E','12','12',null,null,null,null,'1600','6',null,'8WU31M23');
Insert into EC (CODE_EC,LIBELLE_EC,NATURE_EC,HCM,HTD,HTP,HEI,HPRJ,HTPL,CNU,NO_CAT,CODE_EC_PERE,CODE_UE) values ('9WEHMM10','M�thodes pour l''�valuation centr�e utilisateurs','E','12','12',null,null,null,null,'1600','6',null,'9WUHMM06');
Insert into EC (CODE_EC,LIBELLE_EC,NATURE_EC,HCM,HTD,HTP,HEI,HPRJ,HTPL,CNU,NO_CAT,CODE_EC_PERE,CODE_UE) values ('9WEHMM13','Gestion de projet','E','12','12',null,null,null,null,'2700','13',null,null);
Insert into EC (CODE_EC,LIBELLE_EC,NATURE_EC,HCM,HTD,HTP,HEI,HPRJ,HTPL,CNU,NO_CAT,CODE_EC_PERE,CODE_UE) values ('9WEHMM15','Fran�ais Langue Etrang�re','E',null,null,null,null,null,'24','900','8',null,null);
Insert into EC (CODE_EC,LIBELLE_EC,NATURE_EC,HCM,HTD,HTP,HEI,HPRJ,HTPL,CNU,NO_CAT,CODE_EC_PERE,CODE_UE) values ('9WEHMM30','Fran�ais Langue Etrang�re','E',null,null,null,null,null,'24','900','8',null,null);
Insert into EC (CODE_EC,LIBELLE_EC,NATURE_EC,HCM,HTD,HTP,HEI,HPRJ,HTPL,CNU,NO_CAT,CODE_EC_PERE,CODE_UE) values ('9WEHMM19','Int�gration m�thodologique IHM','E',null,'36',null,null,null,null,'2700','6',null,'9WUHMM13');
Insert into EC (CODE_EC,LIBELLE_EC,NATURE_EC,HCM,HTD,HTP,HEI,HPRJ,HTPL,CNU,NO_CAT,CODE_EC_PERE,CODE_UE) values ('9WECPM17','Management et Gestion de projet','E','40',null,null,null,null,null,'0','13',null,'9WUCPM18');
Insert into EC (CODE_EC,LIBELLE_EC,NATURE_EC,HCM,HTD,HTP,HEI,HPRJ,HTPL,CNU,NO_CAT,CODE_EC_PERE,CODE_UE) values ('9WEHMM02','912 Gestion de Projets','E','12','12',null,null,null,null,'2700','13',null,null);
Insert into EC (CODE_EC,LIBELLE_EC,NATURE_EC,HCM,HTD,HTP,HEI,HPRJ,HTPL,CNU,NO_CAT,CODE_EC_PERE,CODE_UE) values ('9WEHMM04','982 Portails Web d''Entreprises','E','12','12',null,null,null,null,'2700','11',null,null);
Insert into EC (CODE_EC,LIBELLE_EC,NATURE_EC,HCM,HTD,HTP,HEI,HPRJ,HTPL,CNU,NO_CAT,CODE_EC_PERE,CODE_UE) values ('9WEHMM18','D�veloppement Web','E','24','24',null,null,null,null,'2700','11',null,null);
Insert into EC (CODE_EC,LIBELLE_EC,NATURE_EC,HCM,HTD,HTP,HEI,HPRJ,HTPL,CNU,NO_CAT,CODE_EC_PERE,CODE_UE) values ('9WECPM20','Droit et fiscalit�','E','40',null,null,null,null,null,'0','7',null,'9WUCPM21');
Insert into EC (CODE_EC,LIBELLE_EC,NATURE_EC,HCM,HTD,HTP,HEI,HPRJ,HTPL,CNU,NO_CAT,CODE_EC_PERE,CODE_UE) values ('9WEHMM21','D�veloppement Mobile','E','24','24',null,null,null,null,'2700','11',null,null);
Insert into EC (CODE_EC,LIBELLE_EC,NATURE_EC,HCM,HTD,HTP,HEI,HPRJ,HTPL,CNU,NO_CAT,CODE_EC_PERE,CODE_UE) values ('9WEHMM22','D�veloppement Web','E','24','24',null,null,null,null,'2700','11',null,null);
Insert into EC (CODE_EC,LIBELLE_EC,NATURE_EC,HCM,HTD,HTP,HEI,HPRJ,HTPL,CNU,NO_CAT,CODE_EC_PERE,CODE_UE) values ('9WEIMM30','Probl�mes de dimensionnement de lots','E','12','12',null,null,null,null,'2700','6',null,'9WUIMM30');
Insert into EC (CODE_EC,LIBELLE_EC,NATURE_EC,HCM,HTD,HTP,HEI,HPRJ,HTPL,CNU,NO_CAT,CODE_EC_PERE,CODE_UE) values ('7WE31M03','Design patterns','E','22','10','12',null,null,null,'2700','6',null,'7WU31M03');
Insert into EC (CODE_EC,LIBELLE_EC,NATURE_EC,HCM,HTD,HTP,HEI,HPRJ,HTPL,CNU,NO_CAT,CODE_EC_PERE,CODE_UE) values ('7WE31M05','Optimisation combinatoire','E','22','14','8',null,null,null,'2700','6',null,'7WU31M05');
Insert into EC (CODE_EC,LIBELLE_EC,NATURE_EC,HCM,HTD,HTP,HEI,HPRJ,HTPL,CNU,NO_CAT,CODE_EC_PERE,CODE_UE) values ('8WE31M06','Introduction � la  fouille de donn�es','E','12','12',null,null,null,null,'2700','6',null,'8WU31M06');
Insert into EC (CODE_EC,LIBELLE_EC,NATURE_EC,HCM,HTD,HTP,HEI,HPRJ,HTPL,CNU,NO_CAT,CODE_EC_PERE,CODE_UE) values ('8WE31M09','Introduction aux mod�les financiers','E','12','12',null,null,null,null,'2700','6',null,'8WU31M09');
Insert into EC (CODE_EC,LIBELLE_EC,NATURE_EC,HCM,HTD,HTP,HEI,HPRJ,HTPL,CNU,NO_CAT,CODE_EC_PERE,CODE_UE) values ('7WE31M06','R�seaux','E','22','8','14',null,null,null,'2700','12',null,'7WU31M06');
Insert into EC (CODE_EC,LIBELLE_EC,NATURE_EC,HCM,HTD,HTP,HEI,HPRJ,HTPL,CNU,NO_CAT,CODE_EC_PERE,CODE_UE) values ('8WE31M10','Syst�mes d'' Information D�cisionnels','E','12','12',null,null,null,null,'2700','6',null,'8WU31M10');
Insert into EC (CODE_EC,LIBELLE_EC,NATURE_EC,HCM,HTD,HTP,HEI,HPRJ,HTPL,CNU,NO_CAT,CODE_EC_PERE,CODE_UE) values ('8WE31M27','Bases de donn�es avanc�es','E','12','12',null,null,null,null,'2700','5',null,'8WU31M15');
Insert into EC (CODE_EC,LIBELLE_EC,NATURE_EC,HCM,HTD,HTP,HEI,HPRJ,HTPL,CNU,NO_CAT,CODE_EC_PERE,CODE_UE) values ('8WE31M04','Techniques de communication et d''expression','E','12','12',null,null,null,null,'7100','13',null,'8WU31M04');
Insert into EC (CODE_EC,LIBELLE_EC,NATURE_EC,HCM,HTD,HTP,HEI,HPRJ,HTPL,CNU,NO_CAT,CODE_EC_PERE,CODE_UE) values ('7WE31M02','Algorithmique et Complexit�','E','22','22',null,null,null,null,'2700','2',null,'7WU31M02');
Insert into EC (CODE_EC,LIBELLE_EC,NATURE_EC,HCM,HTD,HTP,HEI,HPRJ,HTPL,CNU,NO_CAT,CODE_EC_PERE,CODE_UE) values ('8WE31M19','Droit informatique','E','12','12',null,null,null,null,'2700','7',null,'8WU31M19');
Insert into EC (CODE_EC,LIBELLE_EC,NATURE_EC,HCM,HTD,HTP,HEI,HPRJ,HTPL,CNU,NO_CAT,CODE_EC_PERE,CODE_UE) values ('8WE31M21','M�thodologie de la s�curit�','E','12','12',null,null,null,null,'2700','6',null,'8WU31M21');
Insert into EC (CODE_EC,LIBELLE_EC,NATURE_EC,HCM,HTD,HTP,HEI,HPRJ,HTPL,CNU,NO_CAT,CODE_EC_PERE,CODE_UE) values ('8WE31M17','Fonctionnement d''un moteur de rendu 3D','E','12','12',null,null,null,null,'2700','6',null,'8WU31M27');
Insert into EC (CODE_EC,LIBELLE_EC,NATURE_EC,HCM,HTD,HTP,HEI,HPRJ,HTPL,CNU,NO_CAT,CODE_EC_PERE,CODE_UE) values ('8WE31M16','Prototypage d�interfaces par langage de script','E','12','12',null,null,null,null,'2700','6',null,'8WU31M25');
Insert into EC (CODE_EC,LIBELLE_EC,NATURE_EC,HCM,HTD,HTP,HEI,HPRJ,HTPL,CNU,NO_CAT,CODE_EC_PERE,CODE_UE) values ('8WE31M15','La plateforme . NET','E','12','12',null,null,null,null,'2700','3',null,'8WU31M26');
Insert into EC (CODE_EC,LIBELLE_EC,NATURE_EC,HCM,HTD,HTP,HEI,HPRJ,HTPL,CNU,NO_CAT,CODE_EC_PERE,CODE_UE) values ('8WE31M03','Anglais','E',null,null,null,null,null,'24','1100','8',null,'8WU31M03');
Insert into EC (CODE_EC,LIBELLE_EC,NATURE_EC,HCM,HTD,HTP,HEI,HPRJ,HTPL,CNU,NO_CAT,CODE_EC_PERE,CODE_UE) values ('8WE31M08','M�taheuristiques & Algorithmes de recherche stochastique','E','12','12',null,null,null,null,'2700','6',null,'8WU31M08');
Insert into EC (CODE_EC,LIBELLE_EC,NATURE_EC,HCM,HTD,HTP,HEI,HPRJ,HTPL,CNU,NO_CAT,CODE_EC_PERE,CODE_UE) values ('8WE31M14','Initiation � la multimodalit�','E','12','12',null,null,null,null,'2700','6',null,'8WU31M24');
Insert into EC (CODE_EC,LIBELLE_EC,NATURE_EC,HCM,HTD,HTP,HEI,HPRJ,HTPL,CNU,NO_CAT,CODE_EC_PERE,CODE_UE) values ('8WE31M07','Initiation � l''Ordonnancement','E','12','12',null,null,null,null,'2700','6',null,'8WU31M07');
Insert into EC (CODE_EC,LIBELLE_EC,NATURE_EC,HCM,HTD,HTP,HEI,HPRJ,HTPL,CNU,NO_CAT,CODE_EC_PERE,CODE_UE) values ('8WE31M25','Donn�es semi-structur�es et XML','E','12','12',null,null,null,null,'2700','4',null,'8WU31M13');
Insert into EC (CODE_EC,LIBELLE_EC,NATURE_EC,HCM,HTD,HTP,HEI,HPRJ,HTPL,CNU,NO_CAT,CODE_EC_PERE,CODE_UE) values ('8WE31M26','S�mantique des langages de programmation','E','12','12',null,null,null,null,'2700','6',null,'8WU31M14');
Insert into EC (CODE_EC,LIBELLE_EC,NATURE_EC,HCM,HTD,HTP,HEI,HPRJ,HTPL,CNU,NO_CAT,CODE_EC_PERE,CODE_UE) values ('8WE31M22','Introduction � la cryptographie','E','12','12',null,null,null,null,'2700','6',null,'8WU31M16');
Insert into EC (CODE_EC,LIBELLE_EC,NATURE_EC,HCM,HTD,HTP,HEI,HPRJ,HTPL,CNU,NO_CAT,CODE_EC_PERE,CODE_UE) values ('1WCK7M04','Choix de Langues (1/2)','C',null,null,null,null,null,null,null,'8',null,'1WUK7M12');
Insert into EC (CODE_EC,LIBELLE_EC,NATURE_EC,HCM,HTD,HTP,HEI,HPRJ,HTPL,CNU,NO_CAT,CODE_EC_PERE,CODE_UE) values ('2WCK7M05','Choix de langue (1/2)','C',null,null,null,null,null,null,null,'8',null,'2WUK7M04');
Insert into EC (CODE_EC,LIBELLE_EC,NATURE_EC,HCM,HTD,HTP,HEI,HPRJ,HTPL,CNU,NO_CAT,CODE_EC_PERE,CODE_UE) values ('4WEULM14','Gestion du stress et de la performance','E','27',null,null,null,null,null,'7400','7',null,'4WU31M08');
Insert into EC (CODE_EC,LIBELLE_EC,NATURE_EC,HCM,HTD,HTP,HEI,HPRJ,HTPL,CNU,NO_CAT,CODE_EC_PERE,CODE_UE) values ('4WE31M03','Interfaces graphiques','E','8',null,'22',null,null,null,'2700','4',null,'4WU31M02');
Insert into EC (CODE_EC,LIBELLE_EC,NATURE_EC,HCM,HTD,HTP,HEI,HPRJ,HTPL,CNU,NO_CAT,CODE_EC_PERE,CODE_UE) values ('4WE31M07','Remise � niveau Math�matiques','E',null,'45',null,null,null,null,'2700','1',null,'4WU31M05');
Insert into EC (CODE_EC,LIBELLE_EC,NATURE_EC,HCM,HTD,HTP,HEI,HPRJ,HTPL,CNU,NO_CAT,CODE_EC_PERE,CODE_UE) values ('4WEULM24','P�dagogie et didact des sciences/M�tier d''enseignant','E','27',null,null,null,null,null,'0','7',null,'4WU31M08');
Insert into EC (CODE_EC,LIBELLE_EC,NATURE_EC,HCM,HTD,HTP,HEI,HPRJ,HTPL,CNU,NO_CAT,CODE_EC_PERE,CODE_UE) values ('5WE31M05','Syst�me 2','E','6','12','12',null,null,null,'2700','12',null,'5WU31M04');
Insert into EC (CODE_EC,LIBELLE_EC,NATURE_EC,HCM,HTD,HTP,HEI,HPRJ,HTPL,CNU,NO_CAT,CODE_EC_PERE,CODE_UE) values ('6WE31M06','Premiers pas vers l''ing�nierie du logiciel','E','12','12','6',null,null,null,'2700','9',null,'6WU31M04');
Insert into EC (CODE_EC,LIBELLE_EC,NATURE_EC,HCM,HTD,HTP,HEI,HPRJ,HTPL,CNU,NO_CAT,CODE_EC_PERE,CODE_UE) values ('6WC31M02','CHOIX BDD3/CAPES/AD(1/3)','C',null,null,null,null,null,null,null,'0',null,'6WU31M07');
Insert into EC (CODE_EC,LIBELLE_EC,NATURE_EC,HCM,HTD,HTP,HEI,HPRJ,HTPL,CNU,NO_CAT,CODE_EC_PERE,CODE_UE) values ('6WE31M10','Syst�me de gestion de Bases de donn�es','E','12','8','10',null,null,null,'2700','5','6WC31M02',null);
REM INSERTING into ENSEIGNANT
SET DEFINE OFF;
REM INSERTING into ENSEIGNEMENT
SET DEFINE OFF;
REM INSERTING into ETAPE
SET DEFINE OFF;
Insert into ETAPE (CODE_ETAPE,VET,LIBELLE_VET,ID_COMP,CODE_DIPLOME,VDI) values ('1WFMK7','800','L1-Portail Math�matiques Informatique','FB0','3WLFPW1','300');
Insert into ETAPE (CODE_ETAPE,VET,LIBELLE_VET,ID_COMP,CODE_DIPLOME,VDI) values ('3WFM31','800','L2-Informatique','FB0','3WLFINF','200');
Insert into ETAPE (CODE_ETAPE,VET,LIBELLE_VET,ID_COMP,CODE_DIPLOME,VDI) values ('5WFM31','800','L3-Informatique','FB0','3WLFINF','300');
Insert into ETAPE (CODE_ETAPE,VET,LIBELLE_VET,ID_COMP,CODE_DIPLOME,VDI) values ('7WFM31','800','M1-Informatique','FB0','5WMFINF','400');
Insert into ETAPE (CODE_ETAPE,VET,LIBELLE_VET,ID_COMP,CODE_DIPLOME,VDI) values ('9WFMHM','800','M2-Informatique PT G2IHM','FB0','5WMFINF','503');
Insert into ETAPE (CODE_ETAPE,VET,LIBELLE_VET,ID_COMP,CODE_DIPLOME,VDI) values ('9WFMIM','800','M2-Informatique PT D�cisionnelle','FB0','5WMFINF','502');
Insert into ETAPE (CODE_ETAPE,VET,LIBELLE_VET,ID_COMP,CODE_DIPLOME,VDI) values ('9WFMOT','800','M2-Informatique PT SIS','FB0','5WMFINF','504');
REM INSERTING into SEMESTRE
SET DEFINE OFF;
Insert into SEMESTRE (CODE_SEM,LIBELLE_SEM,NO_SEM,CODE_ETAPE,VET) values ('1WSK7M01','Semestre 1 L1 Math/Info','1','1WFMK7','800');
Insert into SEMESTRE (CODE_SEM,LIBELLE_SEM,NO_SEM,CODE_ETAPE,VET) values ('2WSK7M01','Semestre 2 L2 Math/Info','2','1WFMK7','800');
Insert into SEMESTRE (CODE_SEM,LIBELLE_SEM,NO_SEM,CODE_ETAPE,VET) values ('3WS31M01','Semestre 3 L2 Informatique','3','3WFM31','800');
Insert into SEMESTRE (CODE_SEM,LIBELLE_SEM,NO_SEM,CODE_ETAPE,VET) values ('4WS31M01','Semestre 4 L2 Informatique','4','3WFM31','800');
Insert into SEMESTRE (CODE_SEM,LIBELLE_SEM,NO_SEM,CODE_ETAPE,VET) values ('5WS31M01','Semestre 5 L3 Informatique','5','5WFM31','800');
Insert into SEMESTRE (CODE_SEM,LIBELLE_SEM,NO_SEM,CODE_ETAPE,VET) values ('6WS31M01','Semestre 6 L3 Informatique','6','5WFM31','800');
Insert into SEMESTRE (CODE_SEM,LIBELLE_SEM,NO_SEM,CODE_ETAPE,VET) values ('7WS31M01','Semestre 7 M1 Informatique','7','7WFM31','800');
Insert into SEMESTRE (CODE_SEM,LIBELLE_SEM,NO_SEM,CODE_ETAPE,VET) values ('8WS31M01','Semestre 8 M1 Informatique','8','7WFM31','800');
Insert into SEMESTRE (CODE_SEM,LIBELLE_SEM,NO_SEM,CODE_ETAPE,VET) values ('0WSHMM01','Semestre 10','10','9WFMHM','800');
Insert into SEMESTRE (CODE_SEM,LIBELLE_SEM,NO_SEM,CODE_ETAPE,VET) values ('9WSHMM01','SEMESTRE 9','9','9WFMHM','800');
Insert into SEMESTRE (CODE_SEM,LIBELLE_SEM,NO_SEM,CODE_ETAPE,VET) values ('0WSIMM02','Semestre 10','10','9WFMIM','800');
Insert into SEMESTRE (CODE_SEM,LIBELLE_SEM,NO_SEM,CODE_ETAPE,VET) values ('9WSIMM02','Semestre 9','9','9WFMIM','800');
Insert into SEMESTRE (CODE_SEM,LIBELLE_SEM,NO_SEM,CODE_ETAPE,VET) values ('0WSOTM01','SEMESTRE 10','10','9WFMOT','800');
Insert into SEMESTRE (CODE_SEM,LIBELLE_SEM,NO_SEM,CODE_ETAPE,VET) values ('9WSOTM01','SEMESTRE 9','9','9WFMOT','800');
REM INSERTING into SERVICE
SET DEFINE OFF;
REM INSERTING into UE
SET DEFINE OFF;
Insert into UE (CODE_UE,LIBELLE_UE,NATURE_UE,ECTS,CODE_SEM,CODE_UE_PERE) values ('2WUK7M17','UE M�canique du solide','U','6','2WSK7M01','2WCK7M06');
Insert into UE (CODE_UE,LIBELLE_UE,NATURE_UE,ECTS,CODE_SEM,CODE_UE_PERE) values ('2WUK7M01','UE Algorithmique et Programmation 2','U','6','2WSK7M01','2WCK7M03');
Insert into UE (CODE_UE,LIBELLE_UE,NATURE_UE,ECTS,CODE_SEM,CODE_UE_PERE) values ('2WUK7M03','UE210 Outils Math�matiques','U','6','2WSK7M01','2WOK7M01');
Insert into UE (CODE_UE,LIBELLE_UE,NATURE_UE,ECTS,CODE_SEM,CODE_UE_PERE) values ('2WUK7M09','UE Electromagn�tisme','U','6','2WSK7M01','2WCK7M03');
Insert into UE (CODE_UE,LIBELLE_UE,NATURE_UE,ECTS,CODE_SEM,CODE_UE_PERE) values ('2WUK7M10','UE Economie - statistiques','U','6','2WSK7M01','2WCK7M03');
Insert into UE (CODE_UE,LIBELLE_UE,NATURE_UE,ECTS,CODE_SEM,CODE_UE_PERE) values ('2WUK7M11','UE 231 Arithm�tique','U','3','2WSK7M01','2WCK7M04');
Insert into UE (CODE_UE,LIBELLE_UE,NATURE_UE,ECTS,CODE_SEM,CODE_UE_PERE) values ('2WUK7M12','UE232 Compl�ments d''analyse','U','3','2WSK7M01','2WCK7M04');
Insert into UE (CODE_UE,LIBELLE_UE,NATURE_UE,ECTS,CODE_SEM,CODE_UE_PERE) values ('2WUK7M13','UE233 Codage Num�rique : du nombre au pixel','U','3','2WSK7M01','2WCK7M04');
Insert into UE (CODE_UE,LIBELLE_UE,NATURE_UE,ECTS,CODE_SEM,CODE_UE_PERE) values ('2WUK7M14','UE234 M�thodologie de Conception et de Programmation','U','3','2WSK7M01','2WCK7M04');
Insert into UE (CODE_UE,LIBELLE_UE,NATURE_UE,ECTS,CODE_SEM,CODE_UE_PERE) values ('1WUK7M06','UE182 Nombres complexes et g�om�trie','U','3','1WSK7M01','1WCK7M02');
Insert into UE (CODE_UE,LIBELLE_UE,NATURE_UE,ECTS,CODE_SEM,CODE_UE_PERE) values ('1WUK7M07','UE183 Codage num�rique : du nombre au pixel','U','3','1WSK7M01','1WCK7M02');
Insert into UE (CODE_UE,LIBELLE_UE,NATURE_UE,ECTS,CODE_SEM,CODE_UE_PERE) values ('1WUK7M08','UE184 Culture Scientifique','U','3','1WSK7M01','1WCK7M02');
Insert into UE (CODE_UE,LIBELLE_UE,NATURE_UE,ECTS,CODE_SEM,CODE_UE_PERE) values ('1WUK7M09','UE103 Introduction aux syst�mes logiques et num�riques','U','3','1WSK7M01','1WCK7M03');
Insert into UE (CODE_UE,LIBELLE_UE,NATURE_UE,ECTS,CODE_SEM,CODE_UE_PERE) values ('1WUK7M10','UE M�canique du point','U','3','1WSK7M01','1WCK7M03');
Insert into UE (CODE_UE,LIBELLE_UE,NATURE_UE,ECTS,CODE_SEM,CODE_UE_PERE) values ('1WUK7M11','UE Economie S1','U','3','1WSK7M01','1WCK7M03');
Insert into UE (CODE_UE,LIBELLE_UE,NATURE_UE,ECTS,CODE_SEM,CODE_UE_PERE) values ('1WUK7M01','UE101 Calculs et math�matiques','U','6','1WSK7M01',null);
Insert into UE (CODE_UE,LIBELLE_UE,NATURE_UE,ECTS,CODE_SEM,CODE_UE_PERE) values ('1WUK7M02','UE102 Algorithmique et Programmation 1','U','6','1WSK7M01',null);
Insert into UE (CODE_UE,LIBELLE_UE,NATURE_UE,ECTS,CODE_SEM,CODE_UE_PERE) values ('1WUK7M03','UE111 Fondements math�matiques','U','3','1WSK7M01',null);
Insert into UE (CODE_UE,LIBELLE_UE,NATURE_UE,ECTS,CODE_SEM,CODE_UE_PERE) values ('1WUK7M04','UE112 Introduction au Web','U','3','1WSK7M01',null);
Insert into UE (CODE_UE,LIBELLE_UE,NATURE_UE,ECTS,CODE_SEM,CODE_UE_PERE) values ('1WUK7M12','UE190 Comp�tences Transversales','U','6','1WSK7M01',null);
Insert into UE (CODE_UE,LIBELLE_UE,NATURE_UE,ECTS,CODE_SEM,CODE_UE_PERE) values ('2WUK7M05','UE203 M�thodologie de Conception et de Programmation','U','6','2WSK7M01','2WCK7M02');
Insert into UE (CODE_UE,LIBELLE_UE,NATURE_UE,ECTS,CODE_SEM,CODE_UE_PERE) values ('2WUK7M06','UE204 SPI EEA','U','6','2WSK7M01','2WCK7M02');
Insert into UE (CODE_UE,LIBELLE_UE,NATURE_UE,ECTS,CODE_SEM,CODE_UE_PERE) values ('2WUK7M16','UE EEA','U','6','2WSK7M01','2WCK7M06');
Insert into UE (CODE_UE,LIBELLE_UE,NATURE_UE,ECTS,CODE_SEM,CODE_UE_PERE) values ('2WUK7M15','UE235 M�thodo de Conception et de Programmation - Avanc�e','U','3','2WSK7M01','2WCK7M04');
Insert into UE (CODE_UE,LIBELLE_UE,NATURE_UE,ECTS,CODE_SEM,CODE_UE_PERE) values ('2WUK7M07','UE Alg�bre lin�aire 1','U','6','2WSK7M01','2WOK7M02');
Insert into UE (CODE_UE,LIBELLE_UE,NATURE_UE,ECTS,CODE_SEM,CODE_UE_PERE) values ('2WUK7M08','UE212 Analyse 1','U','6','2WSK7M01','2WOK7M02');
Insert into UE (CODE_UE,LIBELLE_UE,NATURE_UE,ECTS,CODE_SEM,CODE_UE_PERE) values ('2WUK7M04','UE290 Comp�tences Transversales','U','6','2WSK7M01',null);
Insert into UE (CODE_UE,LIBELLE_UE,NATURE_UE,ECTS,CODE_SEM,CODE_UE_PERE) values ('3WU31M04','UE ARCHITECTURE DES ORDINATEURS','U','3','3WS31M01','3WC31M01');
Insert into UE (CODE_UE,LIBELLE_UE,NATURE_UE,ECTS,CODE_SEM,CODE_UE_PERE) values ('3WU31M05','UE REMISE A NIVEAU INFORMATIQUE','U','3','3WS31M01','3WC31M01');
Insert into UE (CODE_UE,LIBELLE_UE,NATURE_UE,ECTS,CODE_SEM,CODE_UE_PERE) values ('3WU31M06','UE LANGUE 2 LANGUE 2','U','3','3WS31M01','3WC31M01');
Insert into UE (CODE_UE,LIBELLE_UE,NATURE_UE,ECTS,CODE_SEM,CODE_UE_PERE) values ('3WU31M01','UE MATHS DISCRETES 1','U','6','3WS31M01',null);
Insert into UE (CODE_UE,LIBELLE_UE,NATURE_UE,ECTS,CODE_SEM,CODE_UE_PERE) values ('3WU31M02','UE ALGORITHMIQUE PROGRAMMATION 3','U','6','3WS31M01',null);
Insert into UE (CODE_UE,LIBELLE_UE,NATURE_UE,ECTS,CODE_SEM,CODE_UE_PERE) values ('3WU31M03','UE PROGRAMMATION AVANCEE','U','3','3WS31M01',null);
Insert into UE (CODE_UE,LIBELLE_UE,NATURE_UE,ECTS,CODE_SEM,CODE_UE_PERE) values ('3WU31M07','UE INTRODUCTION AUX BASES DE DONNEES','U','3','3WS31M01',null);
Insert into UE (CODE_UE,LIBELLE_UE,NATURE_UE,ECTS,CODE_SEM,CODE_UE_PERE) values ('3WU31M08','UE OUTILS SYSTEME / RESEAU 1','U','6','3WS31M01',null);
Insert into UE (CODE_UE,LIBELLE_UE,NATURE_UE,ECTS,CODE_SEM,CODE_UE_PERE) values ('3WU31M09','UE LANGUES','U','3','3WS31M01',null);
Insert into UE (CODE_UE,LIBELLE_UE,NATURE_UE,ECTS,CODE_SEM,CODE_UE_PERE) values ('4WU31M04','UE PROBABILITES STATISTIQUES','U','3','4WS31M01','4WC31M01');
Insert into UE (CODE_UE,LIBELLE_UE,NATURE_UE,ECTS,CODE_SEM,CODE_UE_PERE) values ('4WU31M05','UE REMISE A NIVEAU MATHEMATIQUES','U','3','4WS31M01','4WC31M01');
Insert into UE (CODE_UE,LIBELLE_UE,NATURE_UE,ECTS,CODE_SEM,CODE_UE_PERE) values ('4WU31M06','UE LANGUE 2','U','3','4WS31M01','4WC31M01');
Insert into UE (CODE_UE,LIBELLE_UE,NATURE_UE,ECTS,CODE_SEM,CODE_UE_PERE) values ('4WU31M01','UE MATHS DISCRETES 2-LANGAGES FORMELS','U','6','4WS31M01',null);
Insert into UE (CODE_UE,LIBELLE_UE,NATURE_UE,ECTS,CODE_SEM,CODE_UE_PERE) values ('4WU31M02','UE INTERFACES GRAPHIQUES / PROJETS DE SYNTHESE','U','6','4WS31M01',null);
Insert into UE (CODE_UE,LIBELLE_UE,NATURE_UE,ECTS,CODE_SEM,CODE_UE_PERE) values ('4WU31M03','UE SYSTEME 1','U','3','4WS31M01',null);
Insert into UE (CODE_UE,LIBELLE_UE,NATURE_UE,ECTS,CODE_SEM,CODE_UE_PERE) values ('4WU31M07','UE BASES DE LA PROGRAMMATION OBJET','U','6','4WS31M01',null);
Insert into UE (CODE_UE,LIBELLE_UE,NATURE_UE,ECTS,CODE_SEM,CODE_UE_PERE) values ('4WU31M08','UE LIBRE','U','3','4WS31M01',null);
Insert into UE (CODE_UE,LIBELLE_UE,NATURE_UE,ECTS,CODE_SEM,CODE_UE_PERE) values ('4WU31M09','UE LANGUES','U','3','4WS31M01',null);
Insert into UE (CODE_UE,LIBELLE_UE,NATURE_UE,ECTS,CODE_SEM,CODE_UE_PERE) values ('5WU31M05','UE PROGRAMMATION FONCTIONNELLE','U','3','5WS31M01','5WC31M01');
Insert into UE (CODE_UE,LIBELLE_UE,NATURE_UE,ECTS,CODE_SEM,CODE_UE_PERE) values ('5WU31M06','UE XML','U','3','5WS31M01','5WC31M01');
Insert into UE (CODE_UE,LIBELLE_UE,NATURE_UE,ECTS,CODE_SEM,CODE_UE_PERE) values ('5WU31M07','UE COMPRESSION DE DONNEES','U','3','5WS31M01','5WC31M01');
Insert into UE (CODE_UE,LIBELLE_UE,NATURE_UE,ECTS,CODE_SEM,CODE_UE_PERE) values ('5WU31M08','UE INTRODUCTION A LA  ROBOTIQUE','U','3','5WS31M01','5WC31M01');
Insert into UE (CODE_UE,LIBELLE_UE,NATURE_UE,ECTS,CODE_SEM,CODE_UE_PERE) values ('5WU31M01','UE LOGIQUE','U','6','5WS31M01',null);
Insert into UE (CODE_UE,LIBELLE_UE,NATURE_UE,ECTS,CODE_SEM,CODE_UE_PERE) values ('5WU31M02','UE ALGORITHMIQUE / CONCEPTION PROGRAMMATION OBJET AVANCEE','U','9','5WS31M01',null);
Insert into UE (CODE_UE,LIBELLE_UE,NATURE_UE,ECTS,CODE_SEM,CODE_UE_PERE) values ('5WU31M03','UE Bases de donn�es relationnelles et web','U','6','5WS31M01',null);
Insert into UE (CODE_UE,LIBELLE_UE,NATURE_UE,ECTS,CODE_SEM,CODE_UE_PERE) values ('5WU31M04','UE SYSTEME 2','U','3','5WS31M01',null);
Insert into UE (CODE_UE,LIBELLE_UE,NATURE_UE,ECTS,CODE_SEM,CODE_UE_PERE) values ('5WU31M09','UE COMPETENCES TRANSVERSALES','U','3','5WS31M01',null);
Insert into UE (CODE_UE,LIBELLE_UE,NATURE_UE,ECTS,CODE_SEM,CODE_UE_PERE) values ('6WU31M05','UE INTRODUCTION A L''INTELLIGENCE ARTIFICIELLE','U','3','6WS31M01','6WC31M01');
Insert into UE (CODE_UE,LIBELLE_UE,NATURE_UE,ECTS,CODE_SEM,CODE_UE_PERE) values ('6WU31M06','UE INFOGRAPHIE','U','3','6WS31M01','6WC31M01');
Insert into UE (CODE_UE,LIBELLE_UE,NATURE_UE,ECTS,CODE_SEM,CODE_UE_PERE) values ('6WU31M01','UE SECURITE/RESEAU','U','6','6WS31M01',null);
Insert into UE (CODE_UE,LIBELLE_UE,NATURE_UE,ECTS,CODE_SEM,CODE_UE_PERE) values ('6WU31M02','UE OPTIMISATION / MODELISATION','U','6','6WS31M01',null);
Insert into UE (CODE_UE,LIBELLE_UE,NATURE_UE,ECTS,CODE_SEM,CODE_UE_PERE) values ('6WU31M03','UE COMPILATION','U','3','6WS31M01',null);
Insert into UE (CODE_UE,LIBELLE_UE,NATURE_UE,ECTS,CODE_SEM,CODE_UE_PERE) values ('6WU31M04','UE PREMIERS PAS VERS L''INGENIERIE DU LOGICIEL','U','3','6WS31M01',null);
Insert into UE (CODE_UE,LIBELLE_UE,NATURE_UE,ECTS,CODE_SEM,CODE_UE_PERE) values ('6WU31M07','UE OUVERTURE','U','6','6WS31M01',null);
Insert into UE (CODE_UE,LIBELLE_UE,NATURE_UE,ECTS,CODE_SEM,CODE_UE_PERE) values ('6WU31M08','UE Stage','U','3','6WS31M01',null);
Insert into UE (CODE_UE,LIBELLE_UE,NATURE_UE,ECTS,CODE_SEM,CODE_UE_PERE) values ('8WU31M27','UE 836 FONCTIONNEMENT D''UN MOTEUR DE RENDU 3D','U','3','8WS31M01','8WC31M15');
Insert into UE (CODE_UE,LIBELLE_UE,NATURE_UE,ECTS,CODE_SEM,CODE_UE_PERE) values ('8WU31M01','UE 821 INTELLIGENCE ARTIFICIELLE','U','3','8WS31M01',null);
Insert into UE (CODE_UE,LIBELLE_UE,NATURE_UE,ECTS,CODE_SEM,CODE_UE_PERE) values ('8WU31M02','UE 822 REPRESENTATION DES DONNEES VISUELLES','U','3','8WS31M01',null);
Insert into UE (CODE_UE,LIBELLE_UE,NATURE_UE,ECTS,CODE_SEM,CODE_UE_PERE) values ('8WU31M03','UE 801 ANGLAIS','U','2','8WS31M01',null);
Insert into UE (CODE_UE,LIBELLE_UE,NATURE_UE,ECTS,CODE_SEM,CODE_UE_PERE) values ('8WU31M04','UE 802 TECHNIQUES DE COMMUNCATION ET D''EXPRESSION','U','2','8WS31M01',null);
Insert into UE (CODE_UE,LIBELLE_UE,NATURE_UE,ECTS,CODE_SEM,CODE_UE_PERE) values ('7WU31M01','UE 721 ANALYSE ET CONCEPTION DE LOGICIELS','U','4','7WS31M01',null);
Insert into UE (CODE_UE,LIBELLE_UE,NATURE_UE,ECTS,CODE_SEM,CODE_UE_PERE) values ('7WU31M02','UE 722 ALGORITHMIQUE ET COMPLEXITE','U','5','7WS31M01',null);
Insert into UE (CODE_UE,LIBELLE_UE,NATURE_UE,ECTS,CODE_SEM,CODE_UE_PERE) values ('7WU31M03','UE 723 Design patterns','U','4','7WS31M01',null);
Insert into UE (CODE_UE,LIBELLE_UE,NATURE_UE,ECTS,CODE_SEM,CODE_UE_PERE) values ('7WU31M04','UE 724 LOGIQUE ET MODELES DE CALCUL','U','5','7WS31M01',null);
Insert into UE (CODE_UE,LIBELLE_UE,NATURE_UE,ECTS,CODE_SEM,CODE_UE_PERE) values ('7WU31M05','UE 725 OPTIMISATION COMBINATOIRE','U','5','7WS31M01',null);
Insert into UE (CODE_UE,LIBELLE_UE,NATURE_UE,ECTS,CODE_SEM,CODE_UE_PERE) values ('7WU31M06','UE 726 RESEAUX','U','5','7WS31M01',null);
Insert into UE (CODE_UE,LIBELLE_UE,NATURE_UE,ECTS,CODE_SEM,CODE_UE_PERE) values ('7WU31M07','UE 701 ANGLAIS','U','2','7WS31M01',null);
Insert into UE (CODE_UE,LIBELLE_UE,NATURE_UE,ECTS,CODE_SEM,CODE_UE_PERE) values ('8WU31M28','UE 811 INITIATION A LA  RECHERCHE','U','2','8WS31M01','8WC31M01');
Insert into UE (CODE_UE,LIBELLE_UE,NATURE_UE,ECTS,CODE_SEM,CODE_UE_PERE) values ('8WU31M29','UE 812 PROJET EN ENTREPRISE (STAGE EN ALTERNANCE)','U','2','8WS31M01','8WC31M01');
Insert into UE (CODE_UE,LIBELLE_UE,NATURE_UE,ECTS,CODE_SEM,CODE_UE_PERE) values ('8WU31M05','UE 851 OUTILS D''AIDE A LA DECISION','U','3','8WS31M01','8WC31M15');
Insert into UE (CODE_UE,LIBELLE_UE,NATURE_UE,ECTS,CODE_SEM,CODE_UE_PERE) values ('8WU31M06','UE 852 INTRODUCTION A LA FOUILLE DE DONNEES','U','3','8WS31M01','8WC31M15');
Insert into UE (CODE_UE,LIBELLE_UE,NATURE_UE,ECTS,CODE_SEM,CODE_UE_PERE) values ('8WU31M07','UE 853 INITIATION A L''ORDONNANCEMENT','U','3','8WS31M01','8WC31M15');
Insert into UE (CODE_UE,LIBELLE_UE,NATURE_UE,ECTS,CODE_SEM,CODE_UE_PERE) values ('8WU31M08','UE 854 METAHEURISTIQUES & ALGORITHMES DE RECH STOCHASTIQUE','U','3','8WS31M01','8WC31M15');
Insert into UE (CODE_UE,LIBELLE_UE,NATURE_UE,ECTS,CODE_SEM,CODE_UE_PERE) values ('8WU31M09','UE 855 INTRODUCTION AUX MODELES FINANCIERS','U','3','8WS31M01','8WC31M15');
Insert into UE (CODE_UE,LIBELLE_UE,NATURE_UE,ECTS,CODE_SEM,CODE_UE_PERE) values ('8WU31M10','UE 856 SYSTEMES D''INFORMATION DECISIONNELS','U','3','8WS31M01','8WC31M15');
Insert into UE (CODE_UE,LIBELLE_UE,NATURE_UE,ECTS,CODE_SEM,CODE_UE_PERE) values ('8WU31M11','UE 857 GRAPHES D''ATTAQUES ET RESEAUX DE TRANSPORT','U','3','8WS31M01','8WC31M15');
Insert into UE (CODE_UE,LIBELLE_UE,NATURE_UE,ECTS,CODE_SEM,CODE_UE_PERE) values ('8WU31M12','UE 861 TRAITEMENT D''IMAGES ET VISION PAR ORDINATEUR','U','3','8WS31M01','8WC31M15');
Insert into UE (CODE_UE,LIBELLE_UE,NATURE_UE,ECTS,CODE_SEM,CODE_UE_PERE) values ('8WU31M13','UE 862 DONNEES SEMI-STRUCTUREES ET XML','U','3','8WS31M01','8WC31M15');
Insert into UE (CODE_UE,LIBELLE_UE,NATURE_UE,ECTS,CODE_SEM,CODE_UE_PERE) values ('8WU31M14','UE 863 SEMANTIQUE DES LANGAGES DE PROGRAMMATION','U','3','8WS31M01','8WC31M15');
Insert into UE (CODE_UE,LIBELLE_UE,NATURE_UE,ECTS,CODE_SEM,CODE_UE_PERE) values ('8WU31M15','UE 864 BASES DE DONNEES AVANCEES','U','3','8WS31M01','8WC31M15');
Insert into UE (CODE_UE,LIBELLE_UE,NATURE_UE,ECTS,CODE_SEM,CODE_UE_PERE) values ('8WU31M16','UE 841 INTRODUCTION A LA CRYPTOGRAPHIE','U','3','8WS31M01','8WC31M15');
Insert into UE (CODE_UE,LIBELLE_UE,NATURE_UE,ECTS,CODE_SEM,CODE_UE_PERE) values ('8WU31M17','UE 842 INTRODUCTION A LA SECUITE DES SYSTEMES D''INFORMATION','U','3','8WS31M01','8WC31M15');
Insert into UE (CODE_UE,LIBELLE_UE,NATURE_UE,ECTS,CODE_SEM,CODE_UE_PERE) values ('8WU31M18','UE 843 ADMINISTRATION D''UN SYSTEME EN RESEAU','U','3','8WS31M01','8WC31M15');
Insert into UE (CODE_UE,LIBELLE_UE,NATURE_UE,ECTS,CODE_SEM,CODE_UE_PERE) values ('8WU31M19','UE 844 DROIT INFORMATIQUE','U','3','8WS31M01','8WC31M15');
Insert into UE (CODE_UE,LIBELLE_UE,NATURE_UE,ECTS,CODE_SEM,CODE_UE_PERE) values ('8WU31M20','UE 845 ORDONNANCEMENT ET TEMPS-REEL SOUS LINUX','U','3','8WS31M01','8WC31M15');
Insert into UE (CODE_UE,LIBELLE_UE,NATURE_UE,ECTS,CODE_SEM,CODE_UE_PERE) values ('8WU31M21','UE 846 METHODOLOGIE DE LA SECURITE','U','3','8WS31M01','8WC31M15');
Insert into UE (CODE_UE,LIBELLE_UE,NATURE_UE,ECTS,CODE_SEM,CODE_UE_PERE) values ('8WU31M22','UE 831 ERGONOMIE SYSTEMES INTERACTIFS EXPERIENCE UTILISATEUR','U','3','8WS31M01','8WC31M15');
Insert into UE (CODE_UE,LIBELLE_UE,NATURE_UE,ECTS,CODE_SEM,CODE_UE_PERE) values ('8WU31M23','UE 832 PSYCHOLOGIE COGNITIVE ET DIVERSITE ET INTERACTIONS','U','3','8WS31M01','8WC31M15');
Insert into UE (CODE_UE,LIBELLE_UE,NATURE_UE,ECTS,CODE_SEM,CODE_UE_PERE) values ('8WU31M24','UE 833 INITIATION A LA MULTIMODALITE','U','3','8WS31M01','8WC31M15');
Insert into UE (CODE_UE,LIBELLE_UE,NATURE_UE,ECTS,CODE_SEM,CODE_UE_PERE) values ('8WU31M25','UE 834 PROTOTYPAGE D''INTERFACES PAR LANGAGE DE SCRIPT','U','3','8WS31M01','8WC31M15');
Insert into UE (CODE_UE,LIBELLE_UE,NATURE_UE,ECTS,CODE_SEM,CODE_UE_PERE) values ('8WU31M26','UE 835 LA PLATEFORME.NET','U','3','8WS31M01','8WC31M15');
Insert into UE (CODE_UE,LIBELLE_UE,NATURE_UE,ECTS,CODE_SEM,CODE_UE_PERE) values ('0WUHMM01','UE 1002 D�couverte de l�IHM dans des domaines d�application','U','2','0WSHMM01',null);
Insert into UE (CODE_UE,LIBELLE_UE,NATURE_UE,ECTS,CODE_SEM,CODE_UE_PERE) values ('0WUHMM04','UE 1003 Compl�ments de G�nie Logiciel','U',null,'0WSHMM01',null);
Insert into UE (CODE_UE,LIBELLE_UE,NATURE_UE,ECTS,CODE_SEM,CODE_UE_PERE) values ('0WUHMM02','UE 1001 STAGE','U','28','0WSHMM01',null);
Insert into UE (CODE_UE,LIBELLE_UE,NATURE_UE,ECTS,CODE_SEM,CODE_UE_PERE) values ('9WUHMM01','UE 901-1 Anglais','U','2','9WSHMM01',null);
Insert into UE (CODE_UE,LIBELLE_UE,NATURE_UE,ECTS,CODE_SEM,CODE_UE_PERE) values ('9WUHMM02','UE 901-2 Fran�ais Langue Etrang�re','U','2','9WSHMM01',null);
Insert into UE (CODE_UE,LIBELLE_UE,NATURE_UE,ECTS,CODE_SEM,CODE_UE_PERE) values ('9WUHMM03','UE 902 Recherche d''emploi et Cr�ation d''Entreprise','U','2','9WSHMM01',null);
Insert into UE (CODE_UE,LIBELLE_UE,NATURE_UE,ECTS,CODE_SEM,CODE_UE_PERE) values ('9WUHMM51','UE Cr�ation d''entreprise (Alternance)','U','2','9WSHMM01',null);
Insert into UE (CODE_UE,LIBELLE_UE,NATURE_UE,ECTS,CODE_SEM,CODE_UE_PERE) values ('9WUIMM12','UE Isfates : Enseignement interculturel','U','2','9WSHMM01',null);
Insert into UE (CODE_UE,LIBELLE_UE,NATURE_UE,ECTS,CODE_SEM,CODE_UE_PERE) values ('9WUHMM10','UE 978-979 Techniques d�interactions innovantes','U','4','9WSHMM01',null);
Insert into UE (CODE_UE,LIBELLE_UE,NATURE_UE,ECTS,CODE_SEM,CODE_UE_PERE) values ('9WUHMM11','UE 974-975 D�veloppement mobile','U','4','9WSHMM01',null);
Insert into UE (CODE_UE,LIBELLE_UE,NATURE_UE,ECTS,CODE_SEM,CODE_UE_PERE) values ('9WUHMM12','UE 976-977 D�veloppement Web','U','4','9WSHMM01',null);
Insert into UE (CODE_UE,LIBELLE_UE,NATURE_UE,ECTS,CODE_SEM,CODE_UE_PERE) values ('9WUHMM13','UE 911 Int�gration M�thodologique IHM','U','2','9WSHMM01',null);
Insert into UE (CODE_UE,LIBELLE_UE,NATURE_UE,ECTS,CODE_SEM,CODE_UE_PERE) values ('9WUHMM04','UE 969 Ergonomie et facteurs humains pour l�accessibilit�','U','2','9WSHMM01',null);
Insert into UE (CODE_UE,LIBELLE_UE,NATURE_UE,ECTS,CODE_SEM,CODE_UE_PERE) values ('9WUHMM05','UE 970 M�thodes pour la conception centr�e utilisateurs','U','2','9WSHMM01',null);
Insert into UE (CODE_UE,LIBELLE_UE,NATURE_UE,ECTS,CODE_SEM,CODE_UE_PERE) values ('9WUHMM06','UE 971 M�thodes pour l''�valuation centr�e utilisateurs','U','2','9WSHMM01',null);
Insert into UE (CODE_UE,LIBELLE_UE,NATURE_UE,ECTS,CODE_SEM,CODE_UE_PERE) values ('9WUHMM07','UE 972 Techniques de visualisation de donn�es et analyse','U','2','9WSHMM01',null);
Insert into UE (CODE_UE,LIBELLE_UE,NATURE_UE,ECTS,CODE_SEM,CODE_UE_PERE) values ('9WUHMM08','EU 973 Mod�les pour concevoir des syst�mes Interactifs','U','2','9WSHMM01',null);
Insert into UE (CODE_UE,LIBELLE_UE,NATURE_UE,ECTS,CODE_SEM,CODE_UE_PERE) values ('9WUHMM09','UE 912 Gestion de Projet','U','2','9WSHMM01',null);
Insert into UE (CODE_UE,LIBELLE_UE,NATURE_UE,ECTS,CODE_SEM,CODE_UE_PERE) values ('9WUHMM14','UE 901-1 Anglais','U','2','9WSHMM01',null);
Insert into UE (CODE_UE,LIBELLE_UE,NATURE_UE,ECTS,CODE_SEM,CODE_UE_PERE) values ('9WUHMM15','UE 901-2 Fran�ais Langue Etrang�re','U','2','9WSHMM01',null);
Insert into UE (CODE_UE,LIBELLE_UE,NATURE_UE,ECTS,CODE_SEM,CODE_UE_PERE) values ('9WUCPM18','UE ALT 01 Management et gestion de projet','U','2','9WSHMM01',null);
Insert into UE (CODE_UE,LIBELLE_UE,NATURE_UE,ECTS,CODE_SEM,CODE_UE_PERE) values ('9WUCPM19','UE ALT 02 Cr�ation d''entreprise','U','2','9WSHMM01',null);
Insert into UE (CODE_UE,LIBELLE_UE,NATURE_UE,ECTS,CODE_SEM,CODE_UE_PERE) values ('9WUCPM20','UE ALT 03 Marketing','U','2','9WSHMM01',null);
Insert into UE (CODE_UE,LIBELLE_UE,NATURE_UE,ECTS,CODE_SEM,CODE_UE_PERE) values ('9WUCPM21','UE ALT 04 Droit et fiscalit�','U','2','9WSHMM01',null);
Insert into UE (CODE_UE,LIBELLE_UE,NATURE_UE,ECTS,CODE_SEM,CODE_UE_PERE) values ('9WUHMM16','UE Conception Logicielle et Gestion de Projets','U','4','9WSHMM01',null);
Insert into UE (CODE_UE,LIBELLE_UE,NATURE_UE,ECTS,CODE_SEM,CODE_UE_PERE) values ('9WUHMM17','UE Entrep�ts de Donn�es et Portail Web d�Entreprise','U','3','9WSHMM01',null);
Insert into UE (CODE_UE,LIBELLE_UE,NATURE_UE,ECTS,CODE_SEM,CODE_UE_PERE) values ('9WUHMM18','UE S�curit� des R�seaux et des Syst�mes d''Information','U','3','9WSHMM01',null);
Insert into UE (CODE_UE,LIBELLE_UE,NATURE_UE,ECTS,CODE_SEM,CODE_UE_PERE) values ('9WUHMM19','UE983-984 ERP -Introduction au Progiciel de Gestion Int�gr�','U','3','9WSHMM01',null);
Insert into UE (CODE_UE,LIBELLE_UE,NATURE_UE,ECTS,CODE_SEM,CODE_UE_PERE) values ('9WUHMM20','UE 974-975 D�veloppement mobile','U','4','9WSHMM01',null);
Insert into UE (CODE_UE,LIBELLE_UE,NATURE_UE,ECTS,CODE_SEM,CODE_UE_PERE) values ('9WUHMM21','UE 976-977 D�veloppement Web','U','4','9WSHMM01',null);
Insert into UE (CODE_UE,LIBELLE_UE,NATURE_UE,ECTS,CODE_SEM,CODE_UE_PERE) values ('9WUHMM50','UE 911 STG Projet entreprise','U','3','9WSHMM01',null);
Insert into UE (CODE_UE,LIBELLE_UE,NATURE_UE,ECTS,CODE_SEM,CODE_UE_PERE) values ('9WUIMM13','991 Datawarehouse','U','2','9WSIMM02',null);
Insert into UE (CODE_UE,LIBELLE_UE,NATURE_UE,ECTS,CODE_SEM,CODE_UE_PERE) values ('9WUIMM28','998-1 Ordonnancement et Applications','U','2','9WSIMM02',null);
Insert into UE (CODE_UE,LIBELLE_UE,NATURE_UE,ECTS,CODE_SEM,CODE_UE_PERE) values ('9WUIMM33','998-6 Optimisation pour l''industrie','U','2','9WSIMM02',null);
Insert into UE (CODE_UE,LIBELLE_UE,NATURE_UE,ECTS,CODE_SEM,CODE_UE_PERE) values ('0WUIMM01','1002 Applications de l�Informatique D�cisionnelle','U','2','0WSIMM02',null);
Insert into UE (CODE_UE,LIBELLE_UE,NATURE_UE,ECTS,CODE_SEM,CODE_UE_PERE) values ('0WUIMM03','UE 1001 Stage','U','28','0WSIMM02',null);
Insert into UE (CODE_UE,LIBELLE_UE,NATURE_UE,ECTS,CODE_SEM,CODE_UE_PERE) values ('9WUIMM24','997-1 Fouille Informatique des Donn�es','U','2','9WSIMM02',null);
Insert into UE (CODE_UE,LIBELLE_UE,NATURE_UE,ECTS,CODE_SEM,CODE_UE_PERE) values ('9WUIMM22','996-5 Mod�les de Satisfabilit� (SAT)','U','2','9WSIMM02',null);
Insert into UE (CODE_UE,LIBELLE_UE,NATURE_UE,ECTS,CODE_SEM,CODE_UE_PERE) values ('9WUIMM29','998-2 Optimisation et Logiciels - Etude de Cas en Transport','U','2','9WSIMM02',null);
Insert into UE (CODE_UE,LIBELLE_UE,NATURE_UE,ECTS,CODE_SEM,CODE_UE_PERE) values ('9WUIMM05','986 Combinatoire et Algorithmes d''Approximation','U','2','9WSIMM02',null);
Insert into UE (CODE_UE,LIBELLE_UE,NATURE_UE,ECTS,CODE_SEM,CODE_UE_PERE) values ('9WUIMM23','961 BigData','U','2','9WSIMM02',null);
Insert into UE (CODE_UE,LIBELLE_UE,NATURE_UE,ECTS,CODE_SEM,CODE_UE_PERE) values ('9WUIMM01','911 Int�gration M�thodologique OPAL','U','2','9WSIMM02',null);
Insert into UE (CODE_UE,LIBELLE_UE,NATURE_UE,ECTS,CODE_SEM,CODE_UE_PERE) values ('9WUIMM07','988 Optimisation Globale','U','2','9WSIMM02',null);
Insert into UE (CODE_UE,LIBELLE_UE,NATURE_UE,ECTS,CODE_SEM,CODE_UE_PERE) values ('9WUIMM03','902 Professionnalisation OPAL','U','2','9WSIMM02',null);
Insert into UE (CODE_UE,LIBELLE_UE,NATURE_UE,ECTS,CODE_SEM,CODE_UE_PERE) values ('9WUIMM15','993 Conception d''un Syst�me d''Information D�cisionnel','U','2','9WSIMM02',null);
Insert into UE (CODE_UE,LIBELLE_UE,NATURE_UE,ECTS,CODE_SEM,CODE_UE_PERE) values ('9WUIMM21','996-4 Probl�mes de Satisfaction de Contraintes','U','2','9WSIMM02',null);
Insert into UE (CODE_UE,LIBELLE_UE,NATURE_UE,ECTS,CODE_SEM,CODE_UE_PERE) values ('9WUIMM27','997-3 BioInformatique et Datamining','U','2','9WSIMM02',null);
Insert into UE (CODE_UE,LIBELLE_UE,NATURE_UE,ECTS,CODE_SEM,CODE_UE_PERE) values ('9WUIMM30','998-3 Probl�mes de Dimensionnement de lots','U','2','9WSIMM02',null);
Insert into UE (CODE_UE,LIBELLE_UE,NATURE_UE,ECTS,CODE_SEM,CODE_UE_PERE) values ('9WUIMM04','985 R�solution de Mod�les d''Optimisation de Grande Taille','U','2','9WSIMM02',null);
Insert into UE (CODE_UE,LIBELLE_UE,NATURE_UE,ECTS,CODE_SEM,CODE_UE_PERE) values ('9WUIMM31','998-4 Mod�les D�cionnels pour la S�curit� des SI','U','2','9WSIMM02',null);
Insert into UE (CODE_UE,LIBELLE_UE,NATURE_UE,ECTS,CODE_SEM,CODE_UE_PERE) values ('9WUIMM25','972 Techniques de visualisation de donn�es et analyse','U','2','9WSIMM02',null);
Insert into UE (CODE_UE,LIBELLE_UE,NATURE_UE,ECTS,CODE_SEM,CODE_UE_PERE) values ('9WUIMM26','997-2 Algorithmique d''Analyse des Donn�es','U','2','9WSIMM02',null);
Insert into UE (CODE_UE,LIBELLE_UE,NATURE_UE,ECTS,CODE_SEM,CODE_UE_PERE) values ('9WUIMM02','912 Gestion de Projet','U','2','9WSIMM02',null);
Insert into UE (CODE_UE,LIBELLE_UE,NATURE_UE,ECTS,CODE_SEM,CODE_UE_PERE) values ('9WUIMM06','987 Th�orie des Graphes','U','2','9WSIMM02',null);
Insert into UE (CODE_UE,LIBELLE_UE,NATURE_UE,ECTS,CODE_SEM,CODE_UE_PERE) values ('9WUIMM18','996-1 Aide � la D�cision Multicrit�re','U','2','9WSIMM02',null);
Insert into UE (CODE_UE,LIBELLE_UE,NATURE_UE,ECTS,CODE_SEM,CODE_UE_PERE) values ('9WUIMM17','995 Portails Web d''Entreprises 1','U','2','9WSIMM02',null);
Insert into UE (CODE_UE,LIBELLE_UE,NATURE_UE,ECTS,CODE_SEM,CODE_UE_PERE) values ('9WUIMM09','911 Int�gration M�thodologique SID','U','2','9WSIMM02',null);
Insert into UE (CODE_UE,LIBELLE_UE,NATURE_UE,ECTS,CODE_SEM,CODE_UE_PERE) values ('9WUIMM08','990 Th�orie de la complexit� et inapproximabilit�','U','2','9WSIMM02',null);
Insert into UE (CODE_UE,LIBELLE_UE,NATURE_UE,ECTS,CODE_SEM,CODE_UE_PERE) values ('9WUIMM11','902 Professionnalisation SID','U','2','9WSIMM02',null);
Insert into UE (CODE_UE,LIBELLE_UE,NATURE_UE,ECTS,CODE_SEM,CODE_UE_PERE) values ('9WUIMM14','992 S�curit� des syst�mes d''information','U','2','9WSIMM02',null);
Insert into UE (CODE_UE,LIBELLE_UE,NATURE_UE,ECTS,CODE_SEM,CODE_UE_PERE) values ('9WUIMM20','996-3 D�cision pour les Syst�mes Parall�les et Distribu�s','U','2','9WSIMM02',null);
Insert into UE (CODE_UE,LIBELLE_UE,NATURE_UE,ECTS,CODE_SEM,CODE_UE_PERE) values ('9WUIMM10','912 Gestion de Projet','U','2','9WSIMM02',null);
Insert into UE (CODE_UE,LIBELLE_UE,NATURE_UE,ECTS,CODE_SEM,CODE_UE_PERE) values ('9WUIMM16','994 Reporting & Dashboarding : Cr�ation de tableaux de bord','U','2','9WSIMM02',null);
Insert into UE (CODE_UE,LIBELLE_UE,NATURE_UE,ECTS,CODE_SEM,CODE_UE_PERE) values ('9WUIMM19','996-2 D�cision dans l''Incertain','U','2','9WSIMM02',null);
Insert into UE (CODE_UE,LIBELLE_UE,NATURE_UE,ECTS,CODE_SEM,CODE_UE_PERE) values ('9WUIMM32','998-5 Portails Web d''Entreprises 2','U','2','9WSIMM02',null);
Insert into UE (CODE_UE,LIBELLE_UE,NATURE_UE,ECTS,CODE_SEM,CODE_UE_PERE) values ('9WUOTM04','UE 932 S�curit� des syst�mes d''information','U','2','9WSOTM01',null);
Insert into UE (CODE_UE,LIBELLE_UE,NATURE_UE,ECTS,CODE_SEM,CODE_UE_PERE) values ('9WUOTM14','UE 942 Contr�le d''Acc�s','U','2','9WSOTM01',null);
Insert into UE (CODE_UE,LIBELLE_UE,NATURE_UE,ECTS,CODE_SEM,CODE_UE_PERE) values ('9WUOTM13','UE 941 S�curit� des Syst�mes','U','2','9WSOTM01',null);
Insert into UE (CODE_UE,LIBELLE_UE,NATURE_UE,ECTS,CODE_SEM,CODE_UE_PERE) values ('9WUOTM09','UE 937 Tests d''Intrusion','U','2','9WSOTM01',null);
Insert into UE (CODE_UE,LIBELLE_UE,NATURE_UE,ECTS,CODE_SEM,CODE_UE_PERE) values ('9WUOTM03','UE 913 Recherche d''Emploi et Cr�ation d''Entreprise','U','2','9WSOTM01',null);
Insert into UE (CODE_UE,LIBELLE_UE,NATURE_UE,ECTS,CODE_SEM,CODE_UE_PERE) values ('9WUOTM05','UE 933 Politique de la S�curit� d''un Syst�me d''Information','U','2','9WSOTM01',null);
Insert into UE (CODE_UE,LIBELLE_UE,NATURE_UE,ECTS,CODE_SEM,CODE_UE_PERE) values ('9WUOTM02','UE 912 Gestion de Projet','U','2','9WSOTM01',null);
Insert into UE (CODE_UE,LIBELLE_UE,NATURE_UE,ECTS,CODE_SEM,CODE_UE_PERE) values ('9WUOTM07','UE 935 Identity Access Management','U','2','9WSOTM01',null);
Insert into UE (CODE_UE,LIBELLE_UE,NATURE_UE,ECTS,CODE_SEM,CODE_UE_PERE) values ('9WUOTM06','UE 934 Audit de la S�curit� d''un Syst�me d''Information','U','2','9WSOTM01',null);
Insert into UE (CODE_UE,LIBELLE_UE,NATURE_UE,ECTS,CODE_SEM,CODE_UE_PERE) values ('9WUOTM10','UE 938 Malware, retro-ing�nierie','U','2','9WSOTM01',null);
Insert into UE (CODE_UE,LIBELLE_UE,NATURE_UE,ECTS,CODE_SEM,CODE_UE_PERE) values ('0WUOTM01','UE Stage','U','30','0WSOTM01',null);
Insert into UE (CODE_UE,LIBELLE_UE,NATURE_UE,ECTS,CODE_SEM,CODE_UE_PERE) values ('9WUOTM12','UE 940 S�curit� des Donn�es','U','2','9WSOTM01',null);
Insert into UE (CODE_UE,LIBELLE_UE,NATURE_UE,ECTS,CODE_SEM,CODE_UE_PERE) values ('9WUOTM01','UE 911 Int�gration M�thodologiques SSI','U','2','9WSOTM01',null);
Insert into UE (CODE_UE,LIBELLE_UE,NATURE_UE,ECTS,CODE_SEM,CODE_UE_PERE) values ('9WUOTM08','UE 936 R�silience des Syst�mes d''Information','U','2','9WSOTM01',null);
Insert into UE (CODE_UE,LIBELLE_UE,NATURE_UE,ECTS,CODE_SEM,CODE_UE_PERE) values ('9WUOTM11','UE 939 S�curit� des R�seaux','U','2','9WSOTM01',null);
Insert into UE (CODE_UE,LIBELLE_UE,NATURE_UE,ECTS,CODE_SEM,CODE_UE_PERE) values ('1WCK7M02','Choix 1 (1/3)','C',null,'1WSK7M01',null);
Insert into UE (CODE_UE,LIBELLE_UE,NATURE_UE,ECTS,CODE_SEM,CODE_UE_PERE) values ('1WCK7M03','Choix 2 (1/3)','C',null,'1WSK7M01',null);
Insert into UE (CODE_UE,LIBELLE_UE,NATURE_UE,ECTS,CODE_SEM,CODE_UE_PERE) values ('2WCK7M01','Choix d''orientation (1/2)','C',null,'2WSK7M01',null);
Insert into UE (CODE_UE,LIBELLE_UE,NATURE_UE,ECTS,CODE_SEM,CODE_UE_PERE) values ('2WOK7M01','Orientation Info SPI','O',null,'2WSK7M01','2WCK7M01');
Insert into UE (CODE_UE,LIBELLE_UE,NATURE_UE,ECTS,CODE_SEM,CODE_UE_PERE) values ('2WCK7M02','Choix (1/2)','C',null,'2WSK7M01',null);
Insert into UE (CODE_UE,LIBELLE_UE,NATURE_UE,ECTS,CODE_SEM,CODE_UE_PERE) values ('2WCK7M06','Choix UE SPI (1/2)','C',null,'2WSK7M01',null);
Insert into UE (CODE_UE,LIBELLE_UE,NATURE_UE,ECTS,CODE_SEM,CODE_UE_PERE) values ('2WOK7M02','Orientation Math Info','O',null,'2WSK7M01','2WCK7M01');
Insert into UE (CODE_UE,LIBELLE_UE,NATURE_UE,ECTS,CODE_SEM,CODE_UE_PERE) values ('2WCK7M03','Choix info-phys-�co','C',null,'2WSK7M01',null);
Insert into UE (CODE_UE,LIBELLE_UE,NATURE_UE,ECTS,CODE_SEM,CODE_UE_PERE) values ('2WCK7M04','Choix (2/5)','C',null,'2WSK7M01',null);
Insert into UE (CODE_UE,LIBELLE_UE,NATURE_UE,ECTS,CODE_SEM,CODE_UE_PERE) values ('3WC31M01','Choix Architecture Avanc�e/Remise � niveau Info (1/3)','C',null,'3WS31M01',null);
Insert into UE (CODE_UE,LIBELLE_UE,NATURE_UE,ECTS,CODE_SEM,CODE_UE_PERE) values ('4WC31M01','Choix Probabilit�s Statistiques / Remise � niveau Maths(1/3)','C',null,'4WS31M01',null);
Insert into UE (CODE_UE,LIBELLE_UE,NATURE_UE,ECTS,CODE_SEM,CODE_UE_PERE) values ('5WC31M01','Choix Ouverture (1/4)','C',null,'5WS31M01',null);
Insert into UE (CODE_UE,LIBELLE_UE,NATURE_UE,ECTS,CODE_SEM,CODE_UE_PERE) values ('6WC31M01','Choix Introduction � l''IA/Infographie (1/2)','C',null,'5WS31M01',null);
Insert into UE (CODE_UE,LIBELLE_UE,NATURE_UE,ECTS,CODE_SEM,CODE_UE_PERE) values ('8WC31M01','CHOIX 810 Travail d''Etude et de Recherche (1/1)','C',null,'8WS31M01',null);
Insert into UE (CODE_UE,LIBELLE_UE,NATURE_UE,ECTS,CODE_SEM,CODE_UE_PERE) values ('8WC31M15','CHOIX UE / 6 UE AU CHOIX','C',null,'8WS31M01',null);
--------------------------------------------------------
--  Constraints for Table CATEGORIES
--------------------------------------------------------

  ALTER TABLE "CATEGORIES" MODIFY ("CATEGORIE" NOT NULL ENABLE);
  ALTER TABLE "CATEGORIES" ADD PRIMARY KEY ("NO_CAT")
  USING INDEX  ENABLE;
--------------------------------------------------------
--  Constraints for Table COMPOSANTE
--------------------------------------------------------

  ALTER TABLE "COMPOSANTE" MODIFY ("NOM_COMP" NOT NULL ENABLE);
  ALTER TABLE "COMPOSANTE" ADD PRIMARY KEY ("ID_COMP")
  USING INDEX  ENABLE;
--------------------------------------------------------
--  Constraints for Table DIPLOME
--------------------------------------------------------

  ALTER TABLE "DIPLOME" MODIFY ("CODE_DIPLOME" NOT NULL ENABLE);
  ALTER TABLE "DIPLOME" MODIFY ("LIBELLE_DIPLOME" NOT NULL ENABLE);
  ALTER TABLE "DIPLOME" MODIFY ("VDI" NOT NULL ENABLE);
  ALTER TABLE "DIPLOME" MODIFY ("LIBELLE_VDI" NOT NULL ENABLE);
  ALTER TABLE "DIPLOME" ADD PRIMARY KEY ("CODE_DIPLOME", "VDI")
  USING INDEX  ENABLE;
--------------------------------------------------------
--  Constraints for Table EC
--------------------------------------------------------

  ALTER TABLE "EC" MODIFY ("LIBELLE_EC" NOT NULL ENABLE);
  ALTER TABLE "EC" ADD PRIMARY KEY ("CODE_EC")
  USING INDEX  ENABLE;
--------------------------------------------------------
--  Constraints for Table ENSEIGNANT
--------------------------------------------------------

  ALTER TABLE "ENSEIGNANT" MODIFY ("NOM" NOT NULL ENABLE);
  ALTER TABLE "ENSEIGNANT" MODIFY ("PRENOM" NOT NULL ENABLE);
  ALTER TABLE "ENSEIGNANT" MODIFY ("ID_COMP" NOT NULL ENABLE);
  ALTER TABLE "ENSEIGNANT" ADD PRIMARY KEY ("ID_ENS")
  USING INDEX  ENABLE;
--------------------------------------------------------
--  Constraints for Table ENSEIGNEMENT
--------------------------------------------------------

  ALTER TABLE "ENSEIGNEMENT" MODIFY ("CODE_EC" NOT NULL ENABLE);
  ALTER TABLE "ENSEIGNEMENT" MODIFY ("ANNEE" NOT NULL ENABLE);
  ALTER TABLE "ENSEIGNEMENT" ADD PRIMARY KEY ("CODE_EC", "ANNEE")
  USING INDEX  ENABLE;
--------------------------------------------------------
--  Constraints for Table ETAPE
--------------------------------------------------------

  ALTER TABLE "ETAPE" MODIFY ("CODE_ETAPE" NOT NULL ENABLE);
  ALTER TABLE "ETAPE" MODIFY ("VET" NOT NULL ENABLE);
  ALTER TABLE "ETAPE" MODIFY ("LIBELLE_VET" NOT NULL ENABLE);
  ALTER TABLE "ETAPE" ADD PRIMARY KEY ("CODE_ETAPE", "VET")
  USING INDEX  ENABLE;
--------------------------------------------------------
--  Constraints for Table SEMESTRE
--------------------------------------------------------

  ALTER TABLE "SEMESTRE" MODIFY ("LIBELLE_SEM" NOT NULL ENABLE);
  ALTER TABLE "SEMESTRE" MODIFY ("NO_SEM" NOT NULL ENABLE);
  ALTER TABLE "SEMESTRE" ADD PRIMARY KEY ("CODE_SEM")
  USING INDEX  ENABLE;
--------------------------------------------------------
--  Constraints for Table SERVICE
--------------------------------------------------------

  ALTER TABLE "SERVICE" MODIFY ("ID_ENS" NOT NULL ENABLE);
  ALTER TABLE "SERVICE" MODIFY ("CODE_EC" NOT NULL ENABLE);
  ALTER TABLE "SERVICE" MODIFY ("ANNEE" NOT NULL ENABLE);
  ALTER TABLE "SERVICE" ADD PRIMARY KEY ("ID_ENS", "CODE_EC", "ANNEE")
  USING INDEX  ENABLE;
--------------------------------------------------------
--  Constraints for Table UE
--------------------------------------------------------

  ALTER TABLE "UE" MODIFY ("LIBELLE_UE" NOT NULL ENABLE);
  ALTER TABLE "UE" ADD PRIMARY KEY ("CODE_UE")
  USING INDEX  ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table EC
--------------------------------------------------------

  ALTER TABLE "EC" ADD FOREIGN KEY ("NO_CAT")
	  REFERENCES "CATEGORIES" ("NO_CAT") ENABLE;
  ALTER TABLE "EC" ADD FOREIGN KEY ("CODE_EC_PERE")
	  REFERENCES "EC" ("CODE_EC") ENABLE;
  ALTER TABLE "EC" ADD FOREIGN KEY ("CODE_UE")
	  REFERENCES "UE" ("CODE_UE") ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table ENSEIGNANT
--------------------------------------------------------

  ALTER TABLE "ENSEIGNANT" ADD FOREIGN KEY ("ID_COMP")
	  REFERENCES "COMPOSANTE" ("ID_COMP") ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table ENSEIGNEMENT
--------------------------------------------------------

  ALTER TABLE "ENSEIGNEMENT" ADD FOREIGN KEY ("CODE_EC")
	  REFERENCES "EC" ("CODE_EC") ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table ETAPE
--------------------------------------------------------

  ALTER TABLE "ETAPE" ADD FOREIGN KEY ("ID_COMP")
	  REFERENCES "COMPOSANTE" ("ID_COMP") ENABLE;
  ALTER TABLE "ETAPE" ADD FOREIGN KEY ("CODE_DIPLOME", "VDI")
	  REFERENCES "DIPLOME" ("CODE_DIPLOME", "VDI") ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table SEMESTRE
--------------------------------------------------------

  ALTER TABLE "SEMESTRE" ADD FOREIGN KEY ("CODE_ETAPE", "VET")
	  REFERENCES "ETAPE" ("CODE_ETAPE", "VET") ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table SERVICE
--------------------------------------------------------

  ALTER TABLE "SERVICE" ADD FOREIGN KEY ("ID_ENS")
	  REFERENCES "ENSEIGNANT" ("ID_ENS") ENABLE;
  ALTER TABLE "SERVICE" ADD FOREIGN KEY ("CODE_EC", "ANNEE")
	  REFERENCES "ENSEIGNEMENT" ("CODE_EC", "ANNEE") ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table UE
--------------------------------------------------------

  ALTER TABLE "UE" ADD FOREIGN KEY ("CODE_SEM")
	  REFERENCES "SEMESTRE" ("CODE_SEM") ENABLE;
  ALTER TABLE "UE" ADD FOREIGN KEY ("CODE_UE_PERE")
	  REFERENCES "UE" ("CODE_UE") ENABLE;
